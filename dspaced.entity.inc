<?php

/**
 * @file dspaced.entity.inc
 * Extends Drupal's entity classes.
 */

/**
 * Extends EntityFieldQuery which retrieves entities matching a given set of
 * conditions.
 *
 * @note Only tested with node entities.
 */
class DspacedEntityFieldQuery extends EntityFieldQuery {

  /**
   * List of fields to be selected.
   *
   * @var array
   *
   * @see DspacedEntityFieldQuery::fieldSelect()
   */
  public $fieldSelects = array();

  /**
   * Adds fields for selection.
   *
   * @param string $field
   *   Machine name of a field.
   */
  public function fieldSelect($field, $column = 'value', $column_alias = NULL) {
    if (is_scalar($field)) {
      $field_definition = field_info_field($field);
      if (empty($field_definition)) {
        throw new EntityFieldQueryException(t('Unknown field: @field_name', array('@field_name' => $field)));
      }
      $field = $field_definition;
    }
    $index = count($this->fields);
    $this->fields[$index] = $field;

    $this->fieldSelects[] = array(
      'field' => $field,
      'column' => $column,
      'column_alias' => $column_alias
    );
    return $this;
  }

  /**
   * Finishes the query.
   *
   * Adds tags, metaData, range and returns the requested list or count.
   *
   * @param SelectQuery $select_query
   *   A SelectQuery which has entity_type, entity_id, revision_id and bundle
   *   fields added.
   * @param $id_key
   *   Which field's values to use as the returned array keys.
   *
   * @return
   *   See EntityFieldQuery::execute().
   */
  function finishQuery($select_query, $id_key = 'entity_id') {

    // http://drupal.org/node/1226622#comment-6809826 - adds support for IS NULL
    // Iterate through all fields.  If the query is trying to fetch results
    // where a field is null, then alter the query to use a LEFT OUTER join.
    // Otherwise the query will always return 0 results.
    $tables =& $select_query->getTables();
    foreach ($this->fieldConditions as $key => $fieldCondition) {
      if ($fieldCondition['operator'] == 'IS NULL' && isset($this->fields[$key]['storage']['details']['sql'][FIELD_LOAD_CURRENT])) {
        $keys = array_keys($this->fields[$key]['storage']['details']['sql'][FIELD_LOAD_CURRENT]);
        $sql_table = reset($keys);
        foreach ($tables as $table_id => $table) {
          if ($table['table'] == $sql_table) {
            $tables[$table_id]['join type'] = 'LEFT OUTER';
          }
        }
      }
    }

    foreach ($this->tags as $tag) {
      $select_query->addTag($tag);
    }
    foreach ($this->metaData as $key => $object) {
      $select_query->addMetaData($key, $object);
    }
    $select_query->addMetaData('entity_field_query', $this);
    if ($this->range) {
      $select_query->range($this->range['start'], $this->range['length']);
    }
    if ($this->count) {
      return $select_query->countQuery()->execute()->fetchField();
    }
    $return = array();

    $fields = $select_query->getFields();
    $tables = $select_query->getTables();
    foreach ($this->fieldSelects as $field) {
      $table_name = implode('_', array_filter(array('field_data', $field['field']['field_name'])));
      $column = implode('_', array_filter(array($field['field']['field_name'], $field['column'])));
      $column_alias = implode('_', array_filter(array($field['field']['field_name'], $field['column_alias'])));
      foreach ($tables as $table) {
        if ($table['join type'] == 'INNER' && $table['table'] == $table_name) {
          $table_alias = $table['alias'];
        }
      }
      if (isset($table_alias) && $table_alias) {
        $select_query->addField($table_alias, $column, $column_alias);
      }
      else {
        $select_query->addField($fields['entity_id']['table'], $column, $column_alias);
      }
    }

    foreach ($select_query->execute() as $partial_entity) {
      $bundle = isset($partial_entity->bundle) ? $partial_entity->bundle : NULL;
      $entity = entity_create_stub_entity($partial_entity->entity_type, array($partial_entity->entity_id, $partial_entity->revision_id, $bundle));
      foreach ($this->fieldSelects as $field) {
        $column_alias = implode('_', array_filter(array($field['field']['field_name'], $field['column_alias'])));
        if (isset($partial_entity->$column_alias)) {
          $entity->$column_alias = $partial_entity->$column_alias;
        }
      }
      $return[$partial_entity->entity_type][$partial_entity->$id_key] = $entity;
      $this->ordered_results[] = $partial_entity;
    }
    return $return;
  }

}
