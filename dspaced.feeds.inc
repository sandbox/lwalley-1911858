<?php

/**
 * @file
 * Feeds hooks and helper methods for the DSpaced module.
 */

/**
 * Implements hook_feeds_presave().
 *
 * @note Invoked before a feed source is saved, both on create and update.
 */
function dspaced_feeds_presave(FeedsSource $source, $entity) {
  if ($entity->type == 'dspaced_entities_community' && $entity->feeds_item->id == 'dspaced_communities') {
    // Don't create community nodes from dspaced_communities import. We just
    // use it to create the dynamic list for the settings form.
    $entity->feeds_item->skip = TRUE;
  }
  elseif (in_array($entity->type, array('dspaced_entities_item', 'dspaced_entities_community'))) {
    // Any time we import an item or a community, whether its with dspaced_item
    // importer or a dspaced_harvest, or dspaced_community we need to update the
    // Feeds Source URL.
    // @note We hard code part of the Feeds source URL path in the Feeds XML
    //   mapping, but we leave out the domain name and base path and
    //   then prepend it here.
    _dspaced_feeds_source_alter($entity);
    if ($entity->type == 'dspaced_entities_item') {
      // @note We get the bitstream filename from the API response, and then we
      //   prepend the rest of the URL here when self updating an item node.
      _dspaced_dspace_bitstream_url_alter($entity);
    }
  }
}

/**
 * Implements hook_feeds_after_import().
 */
function dspaced_feeds_after_import($source) {
  if ($source->importer->id == 'dspaced_community' && $source->state['process']->created) {
    // Get the nid of the imported community and then trigger its attached
    // dspaced_harvest feed.
    $config = $source->getConfig();
    $dspace_id = str_replace('.xml', '', basename($config['FeedsHTTPFetcher']['source']));
    if ($nodes = _dspaced_find_nodes_by_dspace_id('dspaced_entities_community', array($dspace_id))) {
      if (count($nodes) > 1) {
        watchdog('dspaced', "Found multiple community nodes (@nids) with the same dspace id @id.",
          array('@id' => $dspace_id, '@nids' => implode(array_keys($nodes), ', ')), WATCHDOG_ERROR);
      }
      $node = array_shift($nodes);
      _dspaced_feeds_feed_node_import('dspaced_harvest', $node->nid);
    }
  }
}

/**
 * Implements hook_feeds_after_clear().
 *
 * @note Invoked after a feed source has been cleared of its items. Feed source
 *   can be cleared of items through interface by deselecting a community in the
 *   DSpaced settings form, or by using the 'delete items' node tab. We delete
 *   the community node any time a community is cleared of items regardless of
 *   where the clear was triggered.
 */
function dspaced_feeds_after_clear($source) {
  switch ($source->importer->id) {
    case 'dspaced_harvest':
      // Delete de-selected community nodes, once their items have been cleared.
      if ($nid = $source->feed_nid) {
        if (($node = node_load($nid)) && $node->type == 'dspaced_entities_community') {
          $dspace_id = _dspaced_get_dspace_entity_id($node);
          if (!in_array($dspace_id, variable_get('dspaced_community_dspace_entity_ids', array()))) {
            node_delete($nid);
          }
        }
      }
      break;
  }
}

/**
 * Implements hook_feeds_processor_targets_alter().
 *
 * Provides additional dspaced specific targets for mapping sources to.
 * @see FeedsNodeProcessor::getMappingTargets().
 */
function dspaced_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {
    if ($name == 'body' || $name == 'dspace_pdf_text') {
      // Set callback for altering the body field content from XHTML bitstream
      // URL to the actual content of the bitstream.
      $callback = '_dspaced_bitstream_get_contents_set_target';
    }
    else {
      $info = field_info_field($name);
      switch ($info['type']) {
        case 'image':
          // Set callback for altering image field from bitstream URL to local
          // file.
          $callback = '_dspaced_image_set_target';
          break;
        case 'date':
          // Set callback for checking date field is in an accepted ISO format
          // before inserting into :start or :end subfields
          $callback = '_dspaced_date_set_target';
          break;
      }
    }

    // Assign callbacks for altering the content and/or target field:subfield.
    if (isset($callback) && $callback) {
      $targets[$name . ':dspaced'] = array(
        'name' => 'DSpaced ' . check_plain($instance['label']),
        'callback' => $callback,
        'description' => t('The DSpaced @label field of the node.',
                           array('@label' => $instance['label'])),
        'real_target' => $name,
      );
    }
    unset($callback);
  }
}

/**
 * Set custom bitstream contents field target for :dspaced suffix.
 *
 * Callback for dspaced_feeds_processor_targets_alter().
 *
 * In default dspaced_item Feeds importer the body field target is set to a
 * conditional xpath of a bitstream URL e.g. to the first bitstream URL with
 * mimeType text/xhtml or application/octet-stream with file extension .xhtml,
 * and the dspace_pdf_text field is set to the first bitstream with mimeType
 * text/plain with file extension .pdf.txt.
 *
 * The following callback replaces the parsed value (the bistream URL) with the
 * actual contents of the bitstream.
 *
 * @note Based on files_feeds_set_target and link_feeds_set_target from
 *   Feeds 7.x-2.0-alpha6. Check for modifications whenever Feeds module is
 *   upgraded.
 *
 * @see _dspaced_item_feeds_importer()
 * @see hook_feeds_processor_target_alter()
 * @see file_feeds_set_target()
 * @see link_feeds_set_target()
 *
 */
function _dspaced_bitstream_get_contents_set_target($source, $entity, $target, $value) {
  if (empty($value)) {
    return;
  }

  $request_timeout = _dspaced_get_importer_request_timeout($source);

  // Handle non-multiple value fields.
  if (!is_array($value)) {
    $value = array($value);
  }
  // Make sure each url is absolute and is a FeedsEnclosure instance
  foreach ($value as $k => $v) {
    // @todo: test what happens if $v is already an instance of FeedsEnclosure?
    if (!($v instanceof FeedsEnclosure)) {
      if (is_string($v)) {
        $v = _dspaced_build_bitstream_absolute_url($entity, $v);
        $value[$k] = new FeedsEnclosure($v, file_get_mimetype($v));
        // @note Using importers request_timeout in enclosure requires
        //   patch (https://drupal.org/node/2076065) so we test whether
        //   the patch has been applied by checking that the method exists.
        if (method_exists('FeedsEnclosure', 'setTimeout')) {
          $value[$k]->setTimeout($request_timeout);
        }
      }
      else {
        unset($value[$k]);
      }
    }
  }
  if (empty($value)) {
    return;
  }

  list($field_name, $custom_suffix) = explode(':', $target, 2);
  $info = field_info_field($field_name);
  // Iterate over all values and populate entity.
  $field = isset($entity->$field_name) ? $entity->$field_name : array();
  $i = 0;
  foreach ($value as $v) {
    if ($info['cardinality'] == $i) {
      break;
    }
    $field_value = '';
    try {
      if (($content = $v->getContent()) && ($mime_type = $v->getMimeType())) {
        switch ($mime_type) {
          // @todo Check if we've covered all the relevant mime types
          case 'application/xhtml+xml':
            _dspaced_declare_named_character_entities($content);
            $xhtml = simplexml_load_string($content);
            if ($xhtml && !preg_match('/not found/', $xhtml->head->title)) {
              $field_value = check_markup((string) $xhtml->body->asXML(), 'full_html');
              // Remove excess whitespace and line breaks because they can mess
              // up node summaries.
              $field_value = preg_replace( "/\s+/", " ", $field_value);
              if ($field_value) {
                $field['und'][$i] = array(
                  'value' => trim($field_value),
                  'format' => 'full_html',
                );
                $i++;
              }
            }
            break;
          case 'text/plain':
            // Applies to pdf.txt files
            // @see default _dspaced_item_feeds_importer() definition
            $field_value = check_plain($content);
            if ($field_value) {
              $field['und'][$i] = array(
                'value' => trim($field_value),
              );
              $i++;
            }
            break;
        }
      }
    }
    catch (Exception $e) {
      watchdog_exception('Feeds', $e, nl2br(check_plain($e)));
    }
  }
  $entity->$field_name = $field;

}

/**
 * Set custom image field target for :dspaced suffix.
 *
 * Callback for dspaced_feeds_processor_targets_alter().
 *
 * @note Based on files_feeds_set_target and link_feeds_set_target from
 *   Feeds 7.x-2.0-alpha6. Check for modifications whenever Feeds module is
 *   upgraded.
 *
 * @see hook_feeds_processor_target_alter()
 * @see file_feeds_set_target()
 * @see link_feeds_set_target()
 */
function _dspaced_image_set_target($source, $entity, $target, $value) {
  if (empty($value)) {
    return;
  }

  $request_timeout = _dspaced_get_importer_request_timeout($source);

  // Handle non-multiple value fields.
  if (!is_array($value)) {
    $value = array($value);
  }

  // Make sure each url is absolute and is a FeedsEnclosure instance
  foreach ($value as $k => $v) {
    // @todo: test what happens if $v is already an instance of FeedsEnclosure?
    if (!($v instanceof FeedsEnclosure)) {
      if (is_string($v)) {
        $v = _dspaced_build_bitstream_absolute_url($entity, $v);
        $value[$k] = new FeedsEnclosure($v, file_get_mimetype($v));
        // @note Using importers request_timeout in enclosure requires
        //   patch (https://drupal.org/node/2076065) so we test whether
        //   the patch has been applied by checking that the method exists.
        if (method_exists('FeedsEnclosure', 'setTimeout')) {
          $value[$k]->setTimeout($request_timeout);
        }
      }
      else {
        unset($value[$k]);
      }
    }
  }
  if (empty($value)) {
    return;
  }

  // Determine file destination.
  list($field_name, $custom_suffix) = explode(':', $target, 2);
  list($entity_id, $vid, $bundle_name) = entity_extract_ids($entity->feeds_item->entity_type, $entity);
  $instance_info = field_info_instance($entity->feeds_item->entity_type, $field_name, $bundle_name);
  $info = field_info_field($field_name);
  $data = array();
  if (!empty($entity->uid)) {
    $data[$entity->feeds_item->entity_type] = $entity;
  }
  $destination = file_field_widget_uri($info, $instance_info, $data);

  // Iterate over all values and populate entity.
  $field = isset($entity->$field_name) ? $entity->$field_name : array();
  $i = 0;
  foreach ($value as $v) {
    if ($info['cardinality'] == $i) {
      break;
    }
    try {
      $file = $v->getFile($destination, FILE_EXISTS_REPLACE);
    }
    catch (Exception $e) {
      watchdog_exception('Feeds', $e, nl2br(check_plain($e)));
    }
    if (isset($file) && $file) {
      $field['und'][$i] = (array)$file;
      $field['und'][$i]['display'] = 1;
      $i++;
    }
  }
  $entity->$field_name = $field;
}

/**
 * Check, parse and convert date values as needed prior to switching target to
 * :start or :end date subfields.
 *
 * Callback for dspaced_feeds_processor_targets_alter().
 *
 * @note Assumes desired format is ISO 8601:2004 for Date field type date which
 *   requires partial dates to be padded with zeros.
 *
 * @see http://drupal.org/node/1326872
 *
 * @see hook_feeds_processor_target_alter()
 * @see date_feeds_set_target()
 */
function _dspaced_date_set_target($source, $entity, $target, $value) {
  if (empty($value)) {
    return;
  }

  list($field_name, $custom_suffix) = explode(':', $target, 2);

  // @todo Do we need this? When would it be an instanceof FeedsDateTimeElement?
  if ($value instanceof FeedsDateTimeElement) {
    $value->buildDateField($entity, $field_name);
    return;
  }

  // Parse string and convert to FeedsDateTimeElement
  $info = field_info_field($field_name);
  // Find the field or default Drupal timezone and set as default for dates with
  // missing timezones.
  $default_timezone = new DateTimeZone(date_get_timezone($info['settings']['tz_handling'], date_default_timezone()));
  date_default_timezone_set($default_timezone->getName());
  $input_format = $info['widget']['settings']['input_format_custom'];
  $field = isset($entity->$field_name) ? $entity->$field_name : array();
  $i = 0;
  if (!is_array($value)) {
    $value = array($value);
  }
  foreach ($value as $v) {
    if ($info['cardinality'] == $i) {
      break;
    }
    try {
      if (($datetimes = _dspaced_parse_date($v, $field_name, $entity)) && $datetimes['start']) {
        // Convert dates to field or default Drupal timezone before adding to field
        foreach ($datetimes as $k => $datetime) {
          if ($datetime) $datetimes[$k]->setTimezone($default_timezone);
        }
        $field['und'][$i]['timezone'] = $default_timezone->getName();
        $field['und'][$i]['offset'] = $datetimes['start']->getOffset();
        $field['und'][$i]['date'] = $datetimes['start'];
        $field['und'][$i]['value'] = $datetimes['start']->format($input_format);
        if ($datetimes['end']) {
          $field['und'][$i]['value2'] = $datetimes['end']->format($input_format);
          // We have artificially created start and end datetimes from partial
          // strings, e.g. YY-MM-DD has become YY-MM-DDT00:00:00/YY-MM-DDT23:59:59.
          // However, if the granularity of the field is only YY-MM-DD then our
          // magic is redundant and we'll have ended up with start and end values
          // that are the same. So we do a quick check for that here and remove
          // redundant end values.
          if ($field['und'][$i]['value2'] == $field['und'][$i]['value']) {
            unset($field['und'][$i]['value2']);
          }
          else {
            $field['und'][$i]['date2'] = $datetimes['end'];
          }
        }
        unset($datetimes);
        $i++;
      }
    }
    catch (Exception $e) {
      watchdog_exception('Feeds', $e, nl2br(check_plain($e)));
    }
  }
  $entity->$field_name = $field;
}

/**
 * Helper function to alter Feeds source URL before entity is saved.
 *
 * @see dspaced_feeds_presave()
 *
 * @param object $entity
 *   Drupal entity containing feeds property that needs to be altered.
 *
 * @return NULL
 *   Entity is modified by reference.
 */
function _dspaced_feeds_source_alter($entity) {
  if (isset($entity->feeds['FeedsHTTPFetcher']['source']) && !empty($entity->feeds['FeedsHTTPFetcher']['source'])) {
    $entity->feeds['FeedsHTTPFetcher']['source'] =
      _dspaced_repository_rest_baseurl() .
      $entity->feeds['FeedsHTTPFetcher']['source'];
  }
}

/**
 * Helper function to alter DSpace bitstream URL before entity is saved.
 *
 * @see dspaced_feeds_presave()
 *
 * @param object $node
 *   Drupal entity containing feeds property that needs to be altered.
 * @return NULL
 *   Entity is modified by reference.
 */
function _dspaced_dspace_bitstream_url_alter($entity) {
  if (!property_exists($entity, 'dspace_bitstream_url')) return;
  foreach ($entity->dspace_bitstream_url as $lang => $urls) {
    foreach ($urls as $i => $url) {
      $entity->dspace_bitstream_url[$lang][$i]['url'] =
        _dspaced_build_bitstream_absolute_url($entity, $url['url']);
    }
  }
}

/**
 * Helper function to build an absolute URL for a bistream.
 *
 * @param object $entity
 *   Standard class containing entity data, must include field name
 *   dspace_handle.
 * @param string $file_name
 *   The file name of the bitstream.
 * @return string
 *   Absolute URL to an item's bitstream in DSpace.
 */
function _dspaced_build_bitstream_absolute_url($entity, $file_name) {
  $file_name = str_replace(' ', '%20', $file_name);
  if (parse_url($file_name, PHP_URL_SCHEME)) return $file_name;
  return _dspaced_repository_bitstream_baseurl() .
         _dspaced_get_dspace_handle($entity) . '/' .
         $file_name;
}

/**
 * Helper method to return the base URL of a repository bitstream.
 *
 * @return string
 *   Base URL for a DSpace bitstream.
 */
function _dspaced_repository_bitstream_baseurl() {
  $bitstream_baseurl = &drupal_static(__FUNCTION__);
  if (!isset($bitstream_baseurl)) {
    $parsed_url = parse_url(_dspaced_repository_baseurl());
    if (!isset($parsed_url['path'])) $parsed_url['path'] = '';
    $parsed_url['path'] .= '/bitstream/handle/';
    $bitstream_baseurl = _dspaced_unparse_url($parsed_url);
  }
  return $bitstream_baseurl;
}

/**
 * Helper function to fetch communities from DSpace.
 *
 * @param string $rest_baseurl
 *   The base url for the rest api as entered in
 *   admin/config/dspaced/settings/connect form.
 *
 * @return array
 *   Available communities with community dspace ids as key and community
 *   name as value.
 */
function _dspaced_fetch_communities($rest_baseurl) {
  try {
    $items = _dspaced_feeds_source_standalone_preview('dspaced_communities', "{$rest_baseurl}/communities.xml");
  }
  catch (Exception $e) {
    watchdog('dspaced', "Exception can't fetch communities: @exception",
      array('@exception' => $e->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t("There was an error fetching communities please check the DSpace API service and settings, or check log messages for more details."), 'error');
    $items = array();
  }

  if ($items) {
    $source = feeds_source('dspaced_communities');
    $keys = array('dspace_entity_id' => NULL, 'title' => NULL);
    _dspaced_get_xpathparser_keys($keys, $source->importer()->processor->getMappings());
    foreach ($items as $item) {
      $communities[$item[$keys['dspace_entity_id']]] = $item[$keys['title']];
    }
  }
  return (isset($communities)) ? $communities : array();
}

/**
 * Helper function to get the request timout setting of Feeds importer
 * fetcher.
 *
 * @note If importer request timeout is empty then http_request_get defaults
 *   to using http_request_timeout variable if it exists or 30 seconds.
 *   This is used by target alters for FeedsEnclosure::getContent and
 *   requires FeedsEnclosure patch to allow timeout to be passed to
 *   http_request_get.
 * @see https://drupal.org/node/2076065
 */
function _dspaced_get_importer_request_timeout($source) {
  try {
    $request_timeout = $source->importer()->fetcher->config['request_timeout'];
    return $request_timeout;
  } catch (Exception $e) {
    watchdog('dspaced', 'Exception trying to get request timeout value from ' .
      'Feeds importer: @exception', array('@exception' => $e->getMessage()),
       WATCHDOG_ERROR);
  }
}

/**
 * Helper function to properly declare entities in XHTML documents imported from
 * DSpace.
 *
 * @see _dspaced_bitstream_get_contents_set_target().
 */
function _dspaced_declare_named_character_entities(&$content) {
  static $named_character_entity_declarations = array(
    'nbsp' => '<!ENTITY nbsp "&#160;">',
    'iexcl' => '<!ENTITY iexcl "&#161;">',
    'cent' => '<!ENTITY cent "&#162;">',
    'pound' => '<!ENTITY pound "&#163;">',
    'curren' => '<!ENTITY curren "&#164;">',
    'yen' => '<!ENTITY yen "&#165;">',
    'brvbar' => '<!ENTITY brvbar "&#166;">',
    'sect' => '<!ENTITY sect "&#167;">',
    'uml' => '<!ENTITY uml "&#168;">',
    'copy' => '<!ENTITY copy "&#169;">',
    'ordf' => '<!ENTITY ordf "&#170;">',
    'laquo' => '<!ENTITY laquo "&#171;">',
    'not' => '<!ENTITY not "&#172;">',
    'shy' => '<!ENTITY shy "&#173;">',
    'reg' => '<!ENTITY reg "&#174;">',
    'macr' => '<!ENTITY macr "&#175;">',
    'deg' => '<!ENTITY deg "&#176;">',
    'plusmn' => '<!ENTITY plusmn "&#177;">',
    'sup2' => '<!ENTITY sup2 "&#178;">',
    'sup3' => '<!ENTITY sup3 "&#179;">',
    'acute' => '<!ENTITY acute "&#180;">',
    'micro' => '<!ENTITY micro "&#181;">',
    'para' => '<!ENTITY para "&#182;">',
    'middot' => '<!ENTITY middot "&#183;">',
    'cedil' => '<!ENTITY cedil "&#184;">',
    'sup1' => '<!ENTITY sup1 "&#185;">',
    'ordm' => '<!ENTITY ordm "&#186;">',
    'raquo' => '<!ENTITY raquo "&#187;">',
    'frac14' => '<!ENTITY frac14 "&#188;">',
    'frac12' => '<!ENTITY frac12 "&#189;">',
    'frac34' => '<!ENTITY frac34 "&#190;">',
    'iquest' => '<!ENTITY iquest "&#191;">',
    'Agrave' => '<!ENTITY Agrave "&#192;">',
    'Aacute' => '<!ENTITY Aacute "&#193;">',
    'Acirc' => '<!ENTITY Acirc "&#194;">',
    'Atilde' => '<!ENTITY Atilde "&#195;">',
    'Auml' => '<!ENTITY Auml "&#196;">',
    'Aring' => '<!ENTITY Aring "&#197;">',
    'AElig' => '<!ENTITY AElig "&#198;">',
    'Ccedil' => '<!ENTITY Ccedil "&#199;">',
    'Egrave' => '<!ENTITY Egrave "&#200;">',
    'Eacute' => '<!ENTITY Eacute "&#201;">',
    'Ecirc' => '<!ENTITY Ecirc "&#202;">',
    'Euml' => '<!ENTITY Euml "&#203;">',
    'Igrave' => '<!ENTITY Igrave "&#204;">',
    'Iacute' => '<!ENTITY Iacute "&#205;">',
    'Icirc' => '<!ENTITY Icirc "&#206;">',
    'Iuml' => '<!ENTITY Iuml "&#207;">',
    'ETH' => '<!ENTITY ETH "&#208;">',
    'Ntilde' => '<!ENTITY Ntilde "&#209;">',
    'Ograve' => '<!ENTITY Ograve "&#210;">',
    'Oacute' => '<!ENTITY Oacute "&#211;">',
    'Ocirc' => '<!ENTITY Ocirc "&#212;">',
    'Otilde' => '<!ENTITY Otilde "&#213;">',
    'Ouml' => '<!ENTITY Ouml "&#214;">',
    'times' => '<!ENTITY times "&#215;">',
    'Oslash' => '<!ENTITY Oslash "&#216;">',
    'Ugrave' => '<!ENTITY Ugrave "&#217;">',
    'Uacute' => '<!ENTITY Uacute "&#218;">',
    'Ucirc' => '<!ENTITY Ucirc "&#219;">',
    'Uuml' => '<!ENTITY Uuml "&#220;">',
    'Yacute' => '<!ENTITY Yacute "&#221;">',
    'THORN' => '<!ENTITY THORN "&#222;">',
    'szlig' => '<!ENTITY szlig "&#223;">',
    'agrave' => '<!ENTITY agrave "&#224;">',
    'aacute' => '<!ENTITY aacute "&#225;">',
    'acirc' => '<!ENTITY acirc "&#226;">',
    'atilde' => '<!ENTITY atilde "&#227;">',
    'auml' => '<!ENTITY auml "&#228;">',
    'aring' => '<!ENTITY aring "&#229;">',
    'aelig' => '<!ENTITY aelig "&#230;">',
    'ccedil' => '<!ENTITY ccedil "&#231;">',
    'egrave' => '<!ENTITY egrave "&#232;">',
    'eacute' => '<!ENTITY eacute "&#233;">',
    'ecirc' => '<!ENTITY ecirc "&#234;">',
    'euml' => '<!ENTITY euml "&#235;">',
    'igrave' => '<!ENTITY igrave "&#236;">',
    'iacute' => '<!ENTITY iacute "&#237;">',
    'icirc' => '<!ENTITY icirc "&#238;">',
    'iuml' => '<!ENTITY iuml "&#239;">',
    'eth' => '<!ENTITY eth "&#240;">',
    'ntilde' => '<!ENTITY ntilde "&#241;">',
    'ograve' => '<!ENTITY ograve "&#242;">',
    'oacute' => '<!ENTITY oacute "&#243;">',
    'ocirc' => '<!ENTITY ocirc "&#244;">',
    'otilde' => '<!ENTITY otilde "&#245;">',
    'ouml' => '<!ENTITY ouml "&#246;">',
    'divide' => '<!ENTITY divide "&#247;">',
    'oslash' => '<!ENTITY oslash "&#248;">',
    'ugrave' => '<!ENTITY ugrave "&#249;">',
    'uacute' => '<!ENTITY uacute "&#250;">',
    'ucirc' => '<!ENTITY ucirc "&#251;">',
    'uuml' => '<!ENTITY uuml "&#252;">',
    'yacute' => '<!ENTITY yacute "&#253;">',
    'thorn' => '<!ENTITY thorn "&#254;">',
    'yuml' => '<!ENTITY yuml "&#255;">',
    'fnof' => '<!ENTITY fnof "&#402;">',
    'Alpha' => '<!ENTITY Alpha "&#913;">',
    'Beta' => '<!ENTITY Beta "&#914;">',
    'Gamma' => '<!ENTITY Gamma "&#915;">',
    'Delta' => '<!ENTITY Delta "&#916;">',
    'Epsilon' => '<!ENTITY Epsilon "&#917;">',
    'Zeta' => '<!ENTITY Zeta "&#918;">',
    'Eta' => '<!ENTITY Eta "&#919;">',
    'Theta' => '<!ENTITY Theta "&#920;">',
    'Iota' => '<!ENTITY Iota "&#921;">',
    'Kappa' => '<!ENTITY Kappa "&#922;">',
    'Lambda' => '<!ENTITY Lambda "&#923;">',
    'Mu' => '<!ENTITY Mu "&#924;">',
    'Nu' => '<!ENTITY Nu "&#925;">',
    'Xi' => '<!ENTITY Xi "&#926;">',
    'Omicron' => '<!ENTITY Omicron "&#927;">',
    'Pi' => '<!ENTITY Pi "&#928;">',
    'Rho' => '<!ENTITY Rho "&#929;">',
    'Sigma' => '<!ENTITY Sigma "&#931;">',
    'Tau' => '<!ENTITY Tau "&#932;">',
    'Upsilon' => '<!ENTITY Upsilon "&#933;">',
    'Phi' => '<!ENTITY Phi "&#934;">',
    'Chi' => '<!ENTITY Chi "&#935;">',
    'Psi' => '<!ENTITY Psi "&#936;">',
    'Omega' => '<!ENTITY Omega "&#937;">',
    'alpha' => '<!ENTITY alpha "&#945;">',
    'beta' => '<!ENTITY beta "&#946;">',
    'gamma' => '<!ENTITY gamma "&#947;">',
    'delta' => '<!ENTITY delta "&#948;">',
    'epsilon' => '<!ENTITY epsilon "&#949;">',
    'zeta' => '<!ENTITY zeta "&#950;">',
    'eta' => '<!ENTITY eta "&#951;">',
    'theta' => '<!ENTITY theta "&#952;">',
    'iota' => '<!ENTITY iota "&#953;">',
    'kappa' => '<!ENTITY kappa "&#954;">',
    'lambda' => '<!ENTITY lambda "&#955;">',
    'mu' => '<!ENTITY mu "&#956;">',
    'nu' => '<!ENTITY nu "&#957;">',
    'xi' => '<!ENTITY xi "&#958;">',
    'omicron' => '<!ENTITY omicron "&#959;">',
    'pi' => '<!ENTITY pi "&#960;">',
    'rho' => '<!ENTITY rho "&#961;">',
    'sigmaf' => '<!ENTITY sigmaf "&#962;">',
    'sigma' => '<!ENTITY sigma "&#963;">',
    'tau' => '<!ENTITY tau "&#964;">',
    'upsilon' => '<!ENTITY upsilon "&#965;">',
    'phi' => '<!ENTITY phi "&#966;">',
    'chi' => '<!ENTITY chi "&#967;">',
    'psi' => '<!ENTITY psi "&#968;">',
    'omega' => '<!ENTITY omega "&#969;">',
    'thetasym' => '<!ENTITY thetasym "&#977;">',
    'upsih' => '<!ENTITY upsih "&#978;">',
    'piv' => '<!ENTITY piv "&#982;">',
    'bull' => '<!ENTITY bull "&#8226;">',
    'hellip' => '<!ENTITY hellip "&#8230;">',
    'prime' => '<!ENTITY prime "&#8242;">',
    'Prime' => '<!ENTITY Prime "&#8243;">',
    'oline' => '<!ENTITY oline "&#8254;">',
    'frasl' => '<!ENTITY frasl "&#8260;">',
    'weierp' => '<!ENTITY weierp "&#8472;">',
    'image' => '<!ENTITY image "&#8465;">',
    'real' => '<!ENTITY real "&#8476;">',
    'trade' => '<!ENTITY trade "&#8482;">',
    'alefsym' => '<!ENTITY alefsym "&#8501;">',
    'larr' => '<!ENTITY larr "&#8592;">',
    'uarr' => '<!ENTITY uarr "&#8593;">',
    'rarr' => '<!ENTITY rarr "&#8594;">',
    'darr' => '<!ENTITY darr "&#8595;">',
    'harr' => '<!ENTITY harr "&#8596;">',
    'crarr' => '<!ENTITY crarr "&#8629;">',
    'lArr' => '<!ENTITY lArr "&#8656;">',
    'uArr' => '<!ENTITY uArr "&#8657;">',
    'rArr' => '<!ENTITY rArr "&#8658;">',
    'dArr' => '<!ENTITY dArr "&#8659;">',
    'hArr' => '<!ENTITY hArr "&#8660;">',
    'forall' => '<!ENTITY forall "&#8704;">',
    'part' => '<!ENTITY part "&#8706;">',
    'exist' => '<!ENTITY exist "&#8707;">',
    'empty' => '<!ENTITY empty "&#8709;">',
    'nabla' => '<!ENTITY nabla "&#8711;">',
    'isin' => '<!ENTITY isin "&#8712;">',
    'notin' => '<!ENTITY notin "&#8713;">',
    'ni' => '<!ENTITY ni "&#8715;">',
    'prod' => '<!ENTITY prod "&#8719;">',
    'sum' => '<!ENTITY sum "&#8721;">',
    'minus' => '<!ENTITY minus "&#8722;">',
    'lowast' => '<!ENTITY lowast "&#8727;">',
    'radic' => '<!ENTITY radic "&#8730;">',
    'prop' => '<!ENTITY prop "&#8733;">',
    'infin' => '<!ENTITY infin "&#8734;">',
    'ang' => '<!ENTITY ang "&#8736;">',
    'and' => '<!ENTITY and "&#8869;">',
    'or' => '<!ENTITY or "&#8870;">',
    'cap' => '<!ENTITY cap "&#8745;">',
    'cup' => '<!ENTITY cup "&#8746;">',
    'int' => '<!ENTITY int "&#8747;">',
    'there4' => '<!ENTITY there4 "&#8756;">',
    'sim' => '<!ENTITY sim "&#8764;">',
    'cong' => '<!ENTITY cong "&#8773;">',
    'asymp' => '<!ENTITY asymp "&#8776;">',
    'ne' => '<!ENTITY ne "&#8800;">',
    'equiv' => '<!ENTITY equiv "&#8801;">',
    'le' => '<!ENTITY le "&#8804;">',
    'ge' => '<!ENTITY ge "&#8805;">',
    'sub' => '<!ENTITY sub "&#8834;">',
    'sup' => '<!ENTITY sup "&#8835;">',
    'nsub' => '<!ENTITY nsub "&#8836;">',
    'sube' => '<!ENTITY sube "&#8838;">',
    'supe' => '<!ENTITY supe "&#8839;">',
    'oplus' => '<!ENTITY oplus "&#8853;">',
    'otimes' => '<!ENTITY otimes "&#8855;">',
    'perp' => '<!ENTITY perp "&#8869;">',
    'sdot' => '<!ENTITY sdot "&#8901;">',
    'lceil' => '<!ENTITY lceil "&#8968;">',
    'rceil' => '<!ENTITY rceil "&#8969;">',
    'lfloor' => '<!ENTITY lfloor "&#8970;">',
    'rfloor' => '<!ENTITY rfloor "&#8971;">',
    'lang' => '<!ENTITY lang "&#9001;">',
    'rang' => '<!ENTITY rang "&#9002;">',
    'loz' => '<!ENTITY loz "&#9674;">',
    'spades' => '<!ENTITY spades "&#9824;">',
    'clubs' => '<!ENTITY clubs "&#9827;">',
    'hearts' => '<!ENTITY hearts "&#9829;">',
    'diams' => '<!ENTITY diams "&#9830;">',
    'quot' => '<!ENTITY quot "&#34;">',
    'amp' => '<!ENTITY amp "&#38;">',
    'lt' => '<!ENTITY lt "&#60;">',
    'gt' => '<!ENTITY gt "&#62;">',
    'OElig' => '<!ENTITY OElig "&#338;">',
    'oelig' => '<!ENTITY oelig "&#339;">',
    'Scaron' => '<!ENTITY Scaron "&#352;">',
    'scaron' => '<!ENTITY scaron "&#353;">',
    'Yuml' => '<!ENTITY Yuml "&#376;">',
    'circ' => '<!ENTITY circ "&#710;">',
    'tilde' => '<!ENTITY tilde "&#732;">',
    'ensp' => '<!ENTITY ensp "&#8194;">',
    'emsp' => '<!ENTITY emsp "&#8195;">',
    'thinsp' => '<!ENTITY thinsp "&#8201;">',
    'zwnj' => '<!ENTITY zwnj "&#8204;">',
    'zwj' => '<!ENTITY zwj "&#8205;">',
    'lrm' => '<!ENTITY lrm "&#8206;">',
    'rlm' => '<!ENTITY rlm "&#8207;">',
    'ndash' => '<!ENTITY ndash "&#8211;">',
    'mdash' => '<!ENTITY mdash "&#8212;">',
    'lsquo' => '<!ENTITY lsquo "&#8216;">',
    'rsquo' => '<!ENTITY rsquo "&#8217;">',
    'sbquo' => '<!ENTITY sbquo "&#8218;">',
    'ldquo' => '<!ENTITY ldquo "&#8220;">',
    'rdquo' => '<!ENTITY rdquo "&#8221;">',
    'bdquo' => '<!ENTITY bdquo "&#8222;">',
    'dagger' => '<!ENTITY dagger "&#8224;">',
    'Dagger' => '<!ENTITY Dagger "&#8225;">',
    'permil' => '<!ENTITY permil "&#8240;">',
    'lsaquo' => '<!ENTITY lsaquo "&#8249;">',
    'rsaquo' => '<!ENTITY rsaquo "&#8250;">',
  );

  // Declare found entities in XHTML Doctype element
  preg_match_all('/&(?<names>[A-Za-z0-9]+?);/', $content, $matches);
  $declarations_to_add = array();
  foreach (array_unique($matches['names']) as $named_entity) {
    $declarations_to_add[] = $named_character_entity_declarations[$named_entity];
  }
  $declarations_to_add = "[\n" . join($declarations_to_add, "\n") . "]\n";
  $content = preg_replace('/<!DOCTYPE(.*?)>/', "<!DOCTYPE\${1} {$declarations_to_add}>", $content);
}

