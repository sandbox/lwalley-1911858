ABOUT DSPACED
-------------

DSpaced provides a set of default Feeds importers that can be used
to import items from a DSpace (1.7.x) repository using the DSpace REST API.

WARNING: DSpaced has been tested with a max number of 2,000 items per community,
and with less than 10 communities. A DSpace API 'harvest' response containing
a much larger number of items could fail.

WARNING: Don't try to mamually create or delete community nodes. If you do then
the linked communities settings will get out of synch. Also, feed items are not
deleted by default when when the source feed node is deleted see
https://drupal.org/node/608844. Instead manage communities through the admin
settings form /admin/config/dspaced/settings.

RECOMMENDED PATCHES
-------------------
 * Prevent duplicate nodes from being created by different Feeds importers (ie
   both dspaced_harvest and dspaced_item importers import and create item nodes)
   Issue: https://drupal.org/node/1233142
   Patch: https://drupal.org/files/feeds-existingEntityId_rewrite-1233142-1.patch
   Warning: this causes another problem as yet unresolved, which is that we lose
   the link between the item node and the DSpace community responsible for
   creating it, which in turn prevents removing items when a community is
   removed.
 * Increase time Feeds workers spend on queue
   Issue: https://drupal.org/node/1231332
   Patch: https://drupal.org/files/feeds-queue-1231332-23.patch
 * Increase time Job Scheduler waits before rescheduling a job.
   Issue: https://drupal.org/node/2061647 and patch
   Patch: https://drupal.org/files/job_scheduler-increase-time-to-wait-for-stuck-jobs-2061647-1.patch
 * Allow use of request timeout parameter in FeedsEnclosure::getContent()
   Issue: https://drupal.org/node/2076065 and patch
   Patch: https://drupal.org/files/feeds-enclosure-get-content-timeout-2076065-1.patch
 * Allow file replace on Feeds::getFile()
   Issue: https://drupal.org/node/1171114
   Patch: http://drupal.org/files/feeds-getfile_replace_behaviour_alter-1171114-19.patch

CONFIGURATION
-------------
Summary of configuration steps:
 * Configure cron or poor mans cron.
 * Configure connection to DSpace repository and REST API.
 * Choose communities to import items from.

Settings can be found on the module configuration page
/admin/config/dspaced/settings. You can set the base url of the
DSpace repository's REST API and choose which communities you
want to import items from.

 * Enter the base URL e.g. http://example.com of the DSpace repository.
   The base URL will be used to generate URLs for bitstreams. Bitstream
   URLs are harcoded to use the format /bitstream/handle/ path followed
   by the item handle and the filename.
 * Enter the base URL of the DSpace repository REST API
   e.g. http://example.com/rest
 * Save configuration: a HTTP request is made to the API to
   check the connection using default importer dspaced_communities
   which calls /communities.xml.
   If connection is successful a list of available communities
   is returned with checkboxes.
 * Select the communities you want to import items from.
 * Save configuration: For each selected community the community's
   details are imported using default importer dspaced_community
   which calls /communities/:id.xml, and a community node is
   created (default uses dspaced_entites_community node type)

If you deselect a community and save configuration the community's
node and all it's imported items will be deleted (currently disabled).

IMPORTING ITEMS
---------------
After selecting and importing a community (default node type is
dspaced_entities_community) all item imports are subsequently scheduled
automatically. You can manually bulk import community items by visiting the
import tab on a community node.

The community node import tab uses the dspaced_harvest importer which calls
/harvest.xml?idOnly=true&community=:id to find items. During harvests new
item nodes are created (default node type is dspaced_entities_item) and
populated with just the dspace entity ids. A second import is then triggered to
populate the node with the rest of the item metadata. Once an item node has been
created you can manually populate or update its data using the import tab on
that item node.

The item node import tab uses the dspaced_item importer which calls
/items/:id.xml. Item node imports use FeedsSelfNodeProcessor to self update.

It may take a few cron runs to complete the import of items
after first selecting a community, depending on how many items
are being imported. The following steps are
being taken in the background:

  1. Community node is created.
  2. Harvest import job schedule is added for the community
     node.
  3. Harvest import job is force triggered on an after community
     import hook.
  4. Empty item nodes are created for each item in the harvest results
     (but only if a node with that item's DSpace entity ID does not
     already exist).
  5. An item import job schedule is added for each new item node.
  6. Harvest import job schedule last execution time is updated
     (scheduler will add it to the queue some time in the future and
     a worker will pick it up some time after that to fetch community
     items and add any new ones repeating steps 4-6).
  7. Some time in the future, on cron, Job Scheduler adds the item
     import jobs to the Feeds import queue (maximum of 200 per cron run).
  8. Some time in the future, on cron, worker picks up item import
     job from queue.
  9. Item node is updated.
 10. Item import schedule last execution time is updated (job scheduler
     will add the item import job back to the queue some time in the
     future and a worker will pick it up from the queue some time after
     that repeating steps 8-10).

NOTE: Prioritising empty item nodes from recent harvests would be
ideal but there is no way to do that at the moment. You can instead
increase the frequency of dspace_item imports when you first import
items from a community so that they get added to the queue quicker
then slow it down again once the majority of items are in the queue
to get their full data. However, beware of creating duplicate queue
entries:

Feeds uses Job Scheduler and the core Queue for scheduling and
processing imports and deletions. Job Scheduler will select a
maximum of 200 scheduled jobs each cron run to add to the queue.
When Job Scheduler adds an item to the queue it flags the item
as 'scheduled'. After one hour the 'scheduled' flag is removed
by Job Scheduler, regardless of whether the queue item has
actually been processed or not.

Items in the queue are picked up by Feeds workers and by default
workers will spend 15 seconds processing the queue.

Since we are using FeedsSelfNodeProcessor we will have as many
jobs as there are item nodes so the default one hour wait before
rescheduling combined with the fifteen second working time can
result in duplicate queue entries. To address this there are
a number of things you can do:

 * Increase time Job Scheduler waits before rescheduling a job (see recommended patches)
 * Increase time Feeds workers spend on queue (see recommended patches)
 * Run cron frequently
 * Increase time to wait between imports


DELETING ITEMS
--------------
NOTE - this is currently disabled.

To remove all item nodes imported from a community and the
community node simply deselect the community from the module
settings. To keep the community node and just delete it's
imported items you can use the delete items tab on the
community node.

Deletion of items happens in the background, and may take a
few cron runs to complete.

TODO
----
 * Add node reference between item and communities.
   Even though we import items from specific communities we do not currently
   store the relationship, so we have no way of knowing which communities an
   item belongs to. Note: items can belong to multiple communities.
 * Update linked community id variables when manually delete/update community
   nodes?
 * Quirk of timestamp parameter is that its only to the granularity of seconds,
   but item last modified is has seconds as decimal... so if no items are
   updated it just keeps returning the last item every time... check if there is
   anything we can do about this in the api.
