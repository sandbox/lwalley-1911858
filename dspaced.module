<?php

/**
 * @file dspaced.module
 * Contains implementations of core Drupal hooks and shared helper functions for
 * DSpaced module.
 *
 * @note Implementations of Feeds hooks and helper functions for imports and
 *   clearances are stored in include file dspaced.feeds.
 * @see  dspaced.feeds.inc
 */

/**
 * Implements hook_views_api().
 */
function dspaced_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'dspaced') . '/includes/views',
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function dspaced_ctools_plugin_api($module = '', $api = '') {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => 1);
  }
}

/**
 * Implements hook_hook_info().
 */
function dspaced_hook_info() {
  return array(
    'dspaced_item_feeds_importer_fields_alter' => array(
      'group' => 'dspaced',
    )
  );
}

/**
 * @todo: Implements hook_help().
 * /
function dspaced_help($section) {}
//*/
/**
 * Implements hook_permission().
 */
function dspaced_permission() {
  return array(
    'administer dspaced' => array(
      'title' => t('Administer DSpaced'),
      'description' => t('Configure run and monitor DSpace imports.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function dspaced_menu() {
  $items = array();
  $items['admin/config/dspaced'] = array(
    'title' => 'DSpaced',
    'description' => 'Settings for DSpace integration.',
    'position' => 'right',
    'weight' => 100,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/dspaced/settings'] = array(
    'title' => 'Settings',
    'description' => 'Configure repository connections.',
    'page callback' => 'dspaced_get_wizard',
    'page arguments' => array('admin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'dspaced.admin.inc',
  );
  $items['admin/config/dspaced/settings/%step'] = array(
    'title' => 'Settings',
    'description' => 'Configure repository connections.',
    'page callback' => 'dspaced_get_wizard',
    'page arguments' => array('admin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'dspaced.admin.inc',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function dspaced_theme($existing, $type, $theme, $path) {
  return array(
    'dspaced_status' => array(
      'variables' => array(
        'connection_status' => NULL,
        'linked_communities_count' => NULL,
        'linked_communities_nodes' => NULL,
        'linked_communities_jobs' => NULL,
        'linked_communities_status' => NULL,
        'linked_communities_list' => array(),
        'imported_items_count' => NULL,
        'imported_items_last_item' => NULL,
        'imported_items_status' => NULL,
      ),
    ),
  );
}

/**
 * Implements theme_hook().
 */
function theme_dspaced_status($vars) {
  $output = array();
  $output[] = '<p>' . render($vars['connection_status']) . '</p>';
  $output[] = '<p>' . render($vars['linked_communities_status']) . '</p>';
  $output[] = render($vars['linked_communities_list']);
  $output[] = '<h3>' . render($vars['imported_items_title']) . '</h3>';
  if ($vars['imported_items_status']) {
    $output[] = '<p>' . render($vars['imported_items_status']) . '</p>';
  }
  return implode($output);
}

/**
 * Implements hook_cron().
 */
function dspaced_cron() {
  // Import/schedule new items with blank titles
  $time_start = microtime(TRUE);
  $nids = _dspaced_find_nodes_by_title('dspaced_entities_item');
  if ($nids) {
    $jobs = _dspaced_tools_find_jobs_by_type_and_nid(array('dspaced_item'), $nids);
    foreach ($nids as $i => $nid) {
      if ($i < 10) {
        // Go ahead and import the first 10 regardless
        _dspaced_feeds_feed_node_import('dspaced_item', $nid);
      }
      elseif (!isset($jobs[$nid]) || (isset($jobs[$nid]) && $jobs[$nid]['next'] > REQUEST_TIME)) {
        // Reschedule job but only it doesn't already have a job or if the
        // current job's next time is in the future - otherwise we might keep
        // scheduling the same nodes over and over each cron run
        _dspaced_feeds_feed_node_schedule_import('dspaced_item', $nid);
      }
    }
  }
  $time_end = microtime(TRUE);
  watchdog('dspaced', "@new_items newly created item node(s) with blank titles found; processed in @duration.",
    array(
      '@new_items' => count($nids),
      '@duration' => format_interval(($time_end - $time_start)),
    ),
    WATCHDOG_INFO
  );
}

/**
 * Helper function to return dspace repository's base URL.
 *
 * @return string
 *   Empty string or base URL from system variable
 *   dspaced_repository_baseurl which is set by user in DSpaced system
 *   settings form.
 */
function _dspaced_repository_baseurl() {
  return variable_get('dspaced_repository_baseurl', '');
}

/**
 * Helper function to return dspace repository's REST URL.
 *
 * @return string
 *   Empty string or REST API base URL from system variable
 *   dspaced_repository_rest_baseurl which is set by user in DSpaced system
 *   settings form.
 */
function _dspaced_repository_rest_baseurl() {
  return variable_get('dspaced_repository_rest_baseurl', '');
}

/**
 * Helper function to find nodes by title.
 *
 * @param string $bundle
 *   Machine name of node type to filter by.
 * @param string $title
 *   (optional) Node title. Use NULL to look for nodes with empty title.
 *
 * @return array
 *    An array of node ids.
 */
function _dspaced_find_nodes_by_title($bundle, $title = '') {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundle)
    ->propertyCondition('title', $title);
  $result = $query->execute();
  return (isset($result['node'])) ? array_keys($result['node']) : array();
}

/**
 * Helper function to find nodes by dspace id.
 *
 * @param string $bundle
 *   Machine name of node type to filter by.
 * @param array $dspace_ids
 *   List of integers representing dspace entity ids.
 * @param array $string
 *   (optional) One or two fields to select. Ignored if $node_load is TRUE.
 * @param boolean $node_load
 *  (optional) Whether to load the complete node objects or just return partial
 *  nodes. Defaults to returning partial nodes.
 *
 *  @return array
 *    An array of partial or complete node objects.
 */
function _dspaced_find_nodes_by_dspace_id($bundle, array $dspace_ids, $select = array(), $node_load = FALSE) {
  $query = new DspacedEntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundle)
    ->fieldCondition('dspace_entity_id', 'value', $dspace_ids, 'IN');
  if (!$node_load) {
    $query->fieldSelect('dspace_entity_id');
    foreach ($select as $column) {
      $query->fieldSelect($column);
    }
  }
  $results = $query->execute();
  $return =  array();
  if (isset($results['node'])) {
    if ($node_load) {
      $return = node_load_multiple(array_keys($results['node']));
    }
    else {
      $return = $results['node'];
    }
  }
  return $return;
}

/**
 * Helper function to return loaded community nodes.
 *
 * @return array
 *   Loaded drupal nodes with type dspace_entities_community.
 */
function _dspaced_find_community_nodes() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'dspaced_entities_community');
  $results = $query->execute();
  if (isset($results['node'])) {
    return node_load_multiple(array_keys($results['node']));
  }
}

/**
 * Helper function to return a single value field from a node.
 *
 * @param object $node
 *   Node object containing field to get value from.
 * @param string $field_name
 *   The machine name of the field to get the value from.
 * @return mixed
 *   The field's first value. Assumes field has cardinality == 1.
 *   @todo we could check cardinality and adjust the return appropriately.
 */
function _dspaced_get_raw_field_value($node, $field_name) {
  $field_value = field_get_items('node', $node, $field_name);
  if (is_array($field_value)) {
    $field_value = $field_value[0]['value'];
  }
  return $field_value;
}

/**
 * Helper function to return DSpace handle from node.
 *
 * @param object $node
 *   Node object containing handle field.
 * @return string
 *   DSpace handle value.
 */
function _dspaced_get_dspace_handle($node) {
  return _dspaced_get_raw_field_value($node, 'dspace_handle');
}
/**
 * Helper function to return a DSpace entityId from node.
 *
 * @param object $node
 *   Node object containing dspace entity id field.
 * @return int
 *   DSpace entity ID value.
 */
function _dspaced_get_dspace_entity_id($node) {
  return _dspaced_get_raw_field_value($node, 'dspace_entity_id');
}

/**
 * Helper function to schedule an import of a feed node.
 *
 * @param string $importer_id
 *   A FeedsImporter id as required by feeds_source().
 * @param integer $nid
 *   Node id of the feed.
 * @return null
 *
 * @see FeedsSource::startBackgroundJob()
 */
function _dspaced_feeds_feed_node_schedule_import($importer_id, $nid) {
  $source = feeds_source($importer_id, $nid);
  $job = array(
    'type' => $source->id,
    'id' => $source->feed_nid,
    'period' => 0,
    'periodic' => FALSE,
  );
  JobScheduler::get('feeds_source_import')->set($job);
}

/**
 * Helper function to trigger import of a feed node.
 *
 * @param string $importer_id
 *   A FeedsImporter id as required by feeds_source().
 * @param integer $nid
 *   Node id of the feed.
 * @param boolean $background
 *   TRUE to trigger import as background process, FALSE to let Feed config
 *   determine whether to run in background or start batch job. Use FALSE if
 *   background processing is off in the feed's config and you're calling this
 *   in order to start a batch job in the user interface.
 *
 * @note It seems that importing on cron requires background processing. When
 *   background processing was off for dspaced_item feed, using startImport()
 *   did not seem to work when we called _dspaced_feeds_feed_node_import from
 *   dspaced_cron.
 */
function _dspaced_feeds_feed_node_import($importer_id, $nid, $background = TRUE) {
  try {
    if ($background) {
      // Always runs in background.
      $fake_job = array(
        'type' => $importer_id,
        'id' => $nid,
      );
      feeds_source_import($fake_job);
    }
    else {
      // May still run in background; it checks source config then decides.
      $source = feeds_source($importer_id, $nid);
      $source->startImport();
    }
  }
  catch (Exception $e) {
    watchdog('dspaced', "Caught exception trying to import node @nid with importer @importer_id: @emessage",
      array(
        '@nid' => $nid,
        '@importer' => $importer_id,
        '@emessage' => $e->getMessage()
      ),
      WATCHDOG_ERROR
    );
  }
}

/**
 * Helper function to trigger clearing of a feed node.
 *
 * @param string $importer_id
 *   A FeedsImporter id as required by feeds_source().
 * @param integer $nid
 *   Node id of the feed.
 * @param boolean $background
 *   TRUE to trigger clear as background process, FALSE to let Feed config
 *   determine whether to run in background or start batch job. Use FALSE if
 *   background processing is off in the feed's config and you're calling this
 *   in order to start a batch job in the user interface.
 */
function _dspaced_feeds_feed_node_clear($importer_id, $nid, $background = TRUE) {
  if ($background) {
    // Always runs in background.
    $fake_job = array(
      'type' => $importer_id,
      'id' => $nid,
    );
    feeds_source_clear($fake_job);
  }
  else {
    // May still run in background; it checks source config then decides.
    $source = feeds_source($importer_id, $nid);
    $source->startClear();
  }
}

/**
 * Helper function to start a standalone feeds source import.
 *
 * @note Only intended for use with HTTP fetcher.
 *
 * @param string $importer_id
 *   A FeedsImporter id as required by feeds_source().
 *
 * @param string $url
 *   The FeedsFetcher url.
 */
function _dspaced_feeds_source_standalone_import($importer_id, $url) {
  try {
    $source = feeds_source($importer_id);
    $config = $source->getConfigFor($source->importer->fetcher);
    $config['source'] = $url;
    $source->setConfigFor($source->importer->fetcher, $config);
    $source->save();
    $source->startImport();
  }
  catch (Exception $e) {
    watchdog('dspaced', "Caught exception trying to run standalone import for importer @importer_id: @emessage",
      array(
        '@importer' => $importer_id,
        '@emessage' => $e->getMessage()
      ),
      WATCHDOG_ERROR
    );
  }
}

/**
 * Helper function to preview a standalone feeds source import.
 *
 * @note Only intended for use with HTTP fetcher.
 *
 * @param string $importer_id
 *   A FeedsImporter id as required by feeds_source().
 *
 * @param string $url
 *   The FeedsFetcher url.
 *
 * @return array
 *   Array of items returned by fetcher and parsed.
 */
function _dspaced_feeds_source_standalone_preview($importer_id, $url) {
  $source = feeds_source($importer_id);
  $config = $source->getConfigFor($source->importer->fetcher);
  $config['source'] = $url;
  $source->setConfigFor($source->importer->fetcher, $config);
  $source->save();
  $items = $source->preview()->items;
  return $items;
}

/**
 * Helper function to find the xpathparser keys of a FeedsNodeProcessor mapping
 * target.
 *
 * @param array $target
 *   Array of null values keyed with mapping target name. Passed by reference
 *   and will be updated with keys e.g. array('target' => NULL) will be updated
 *   to array('target' => 'key').
 *
 * @param array $mappings
 *   Processor mapping e.g. from $source->importer()->processor->getMappings().
 *
 * @return array
 *   Array of targets and their mapping source key (xpathparser key) if found.
 */
function _dspaced_get_xpathparser_keys(array &$targets, array $mappings) {
  foreach ($mappings as $mapping) {
    if (in_array($mapping['target'], array_keys($targets))) {
      $targets[$mapping['target']] = $mapping['source'];
    }
  }
}
/**
 * Helper function to ping URL and return status message of http response.
 *
 * @param string $url
 *   URL to send in request.
 * @param boolean $trust_codes
 *   TRUE or FALSE depending on whether we can trust the status codes in the
 *   response header.
 * @param boolean $api_ping
 *   TRUE or FALSE depending on whether this is checking the DSpace API or not.
 *   If so we assume its pinging communities.xml and we check the response is as
 *   expected; we don't rely on status codes because some APIs will redirect on
 *   incorrect URLs returning a 200 instead of a 404.
 * @return string
 *   Status message in the format 'code status message'.
 *
 */
function _dspaced_get_http_status_message($url, $trust_codes = FALSE, $api_ping = FALSE) {
  $response = drupal_http_request($url);
  $message = '';
  if ($api_ping) {
    // @note This is an api check so we assume we're trying to ping communities.xml
    // @see dspaced.admin.inc
    if (!preg_match('/<communities_collection entityPrefix="communities">/', $response->data)) {
      $message = t('404 Not Found');
    }
  }
  if (!$trust_codes && isset($response->data)) {
    // DSpace doesn't return the correct reponse code for page not found so we
    // check the content of the response to see if its what we expect
    if (preg_match('/<title>(.*?)<\/title>/', $response->data, $match)) {
      switch ($match[1]) {
        case 'Page not found':
          $message = t('404 Not Found');
          break;
      }
    }
  }
  if (!$message) {
    if (isset($response->error)) {
      switch ($response->code) {
        case 0:
          $message = t("@code Domain Name Resolution Failed",
            array('@code' => $response->code));
          break;
        default:
          $message = "{$response->code} {$response->error}";
          break;
      }
    }
    else {
      $message = "{$response->code} {$response->status_message}";
    }
  }
  return $message;
}

/**
 * Helper function to check status message is ok.
 *
 * @param string $message
 *   Should contain HTTP response code e.g. '200 OK'.
 */
function _dspaced_http_status_message_is_ok($message) {
  return ((int) $message >= 200 && (int) $message < 400);
}

/**
 * Helper function to return job schedules given types and nids.
 *
 * @param array $types
 *   Array of strings containing feeds importer ids.
 * @param array $nids
 *   Array of node ids.
 *
 * @return array
 *   Array of standard objects containing scheduler info.
 */
function _dspaced_tools_find_jobs_by_type_and_nid(array $types, array $nids) {
  $jobs = array();
  $result = db_query('SELECT * ' .
                     'FROM {job_schedule} ' .
                     'WHERE name IN(:name) ' .
                     'AND type = :type ' .
                     'AND id IN(:ids)',
                   array(':name' => 'feeds_source_import',
                     ':type' => $types,
                     ':ids' => $nids));
  if ($result) {
    while ($row = $result->fetchAssoc()) {
      $jobs[$row['id']] = $row;
    }
  }
  return $jobs;
}

/**
 * Helper function to find the most recently modified item.
 *
 * @note By last modified date in DSpace not Drupal.
 */
function _dspaced_find_lastmodified_item_node() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'dspaced_entities_item')
    ->fieldOrderBy('dspace_lastmodified', 'value', 'DESC')
    ->range(0, 1);

  $result = $query->execute();

  $nodes = array();
  if (isset($result['node'])) {
    $nodes = entity_load('node', array_keys($result['node']));
  }
  return array_shift($nodes);
}

/**
 * Helper function to retrieve object from ctools cache.
 *
 * @param string $name
 *   Name of the cached object to retrieve.
 *
 * @return object
 *   Cache standard class object.
 */
function _dspaced_get_page_cache($name) {
  ctools_include('object-cache');
  $cache = ctools_object_cache_get('dspaced', $name);

  if (!$cache) {
    $cache = new stdClass();
    $cache->locked = ctools_object_cache_test('dspaced', $name);
  }

  return $cache;
}

/**
 * Helper function to create or update object in ctools cache.
 *
 * @param string $name
 *   Name of the object to cache.
 *
 * @param object $data
 *   Standard class object to be cached.
 *
 * @return null
 */
function _dspaced_set_page_cache($name, $data) {
  ctools_include('object-cache');
  $cache = ctools_object_cache_set('dspaced', $name, $data);
}

/**
 * Helper function to remove object from ctools cache.
 *
 * @param string $name
 *   Name of the cached object to remove.
 *
 * @return null
 */
function _dspaced_clear_page_cache($name) {
  ctools_include('object-cache');
  ctools_object_cache_clear('dspaced', $name);
}

/**
 * Helper function to parse date strings from DSpace and convert into complete
 * ISO 8601 format, and to range if partial dates provided.
 *
 * @param String $date
 *   Assumes date string in one of the following ISO 8601 formats (DATE_W3C)
 *   and may be partial (i.e. parts starting from right to left may be missing,
 *   minimum is [Y] indicating a millenium):
 *     YYYY-MM-DDThh:mm:ssZ
 *     YYYY-MM-DDThh:mm:ss-hh:mm
 *     YYYY-MM-DDThh:mm:ss+hh:mm
 *   Assumes intervals (intervening time between two time points) are
 *   represented by a [/] separator
 *   (e.g. YYYY-MM-DDThh:mm:ssZ/YYYY-MM-DDThh:mm:ssZ). If time zone is not
 *   included we assume UTC.
 *   Assumes intervals are in CAL_GREGORIAN.
 *
 * @return Array
 *   Dates in ISO format as array of subfields with keys start and end.
 *
 * @note Does not currently support other ISO 8601 formats or other date strings
 *   and since DSpace dates can be user entered this means some dates may be
 *   ignored and not imported. Does not solve the issue of uncertainty e.g.
 *   "Probably after 1928" however since partial iso regex can appear
 *   anywhere in the string it should pick up the year from "Probably after 1928".
 *
 * @todo Figure out how to handle uncertainty in dates e.g. "Probably after 1928".
 */
function _dspaced_parse_date($date, $field_name, $entity) {

  // Expects ISO intervals as expressed by / separator.
  $points = array_filter(explode('/', trim($date)), '_dspaced_parse_date_filter');

  // More than two interval points indicates $date is not in expected ISO format
  if (!$points || count($points) > 2) return NULL;

  $complete_iso_regex = '/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(Z|[-+]\d{2}:\d{2})/';
  $partial_iso_regex = '/(\d{1,4})-?(\d{0,2})-?(\d{0,2})T?(\d{0,2}):?(\d{0,2}):?(\d{0,2})(Z|[-+]\d{2}:\d{2}|[-+]\d{2})?/';

  $iso_dates = array();
  foreach ($points as $point) {
    if (preg_match($complete_iso_regex, $point)) {
      // DSpace interval point is already in complete ISO format.
      $iso_dates[] = $point;
      continue;
    }

    if (preg_match($partial_iso_regex, $point, $match)) {
      // DSpace interval point has partially matched ISO format.
      // 1 => Millenium [Y], Century [YY], Decade [YYY], Year [YYYY]
      // 2 => Month
      // 3 => Day
      // 4 => Hour
      // 5 => Minutes
      // 6 => Seconds
      // 7 => Timezone or difference from UTC

      $match = array_pad($match, 8, NULL);
      $partial = $match;
      $match[1] = str_pad($match[1], 4, '#');
      for ($i = 2; $i <= 6; $i++) {
        $match[$i] = str_pad($match[$i], 2, '#');
      }

      // Create range values based on position of first missing value
      // @note We don't care about validation at this point, we'll check the
      //   dates later.
      $pos = strpos("{$match[1]}-{$match[2]}-{$match[3]}T{$match[4]}:{$match[5]}:{$match[6]}", '#');
      switch ($pos) {
        case '1': // Millenium Y#
        case '2': // Century YY#
        case '3': // Decade YYY#
        case '5': // Year YYYY-#
          $iso_dates[] = str_pad($partial[1], 4, '0') . '-01-01T00:00:00';
          $iso_dates[] = str_pad($partial[1], 4, '9') . '-12-31T23:59:59';
          break;
        case '6': // Month range YYYY-M# (not iso 8601 but assume range intended)
          switch ($partial[2]) {
            case '0':
              $start_month = '01';
              $end_month = '09';
              break;
            case '1':
              $start_month = '10';
              $end_month = '12';
              break;
          }
          if ($start_month) {
            $iso_dates[] = "{$partial[1]}-{$start_month}-01T00:00:00";
          }

          if ($end_month && ($days_in_month = cal_days_in_month(CAL_GREGORIAN, $end_month, $partial[1]))) {
            $iso_dates[] = "{$partial[1]}-{$end_month}-{$days_in_month}T23:59:59";
          }
          elseif ($start_month && ($days_in_month = cal_days_in_month(CAL_GREGORIAN, $start_month, $partial[1]))) {
            $iso_dates[] = "{$partial[1]}-{$start_month}-{$days_in_month}T23:59:59";
          }
          unset($start_month);
          unset($end_month);
          unset($days_in_month);
          break;
        case '8': // Month YYYY-MM-#
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-01T00:00:00";
          if ($days_in_month = cal_days_in_month(CAL_GREGORIAN, $partial[2], $partial[1])) {
            $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$days_in_month}T23:59:59";
          }
          unset($days_in_month);
          break;
        case '9': // Day range YYYY-MM-D# (not iso 8601 but assume range intended)
          if ($days_in_month = cal_days_in_month(CAL_GREGORIAN, $partial[2], $partial[1])) {
            switch ($partial[3]) {
              case '0':
                $start_day = '01';
                $end_day = '09';
                break;
              case '1':
              case '2':
              case '3':
                $start_day = "{$partial[3]}0";
                $end_day = "{$partial[3]}9";
                break;
            }
            if ($start_day > $days_in_month) $start_day = $days_in_month;
            if ($end_day > $days_in_month) $end_day = $days_in_month;

            if ($start_day) $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$start_day}T00:00:00";
            if ($end_day) $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$end_day}T23:59:59";

            unset($start_day);
            unset($end_day);
          }
          unset($days_in_month);
          break;
        case '11': // Day YYYY-MM-DDT#
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T00:00:00";
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T23:59:59";
          break;
        case '12': // Hour range YYYY-MM-DDTh# (not iso 8601 but assume range intended)
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}0:00:00";
          $end_hour = (intval("{$partial[4]}9") > 23) ? '23' : "{$partial[4]}9";
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$end_hour}:59:59";
          unset($end_hour);
          break;
        case '14': // Hour YYYY-MM-DDThh:#
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:00:00";
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:59:59";
          break;
        case '15': // Minute range YYYY-MM-DDThh:m# (not iso 8601 but assume range intended)
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:{$partial[5]}0:00";
          $end_minute = (intval("{$partial[5]}9") > 59) ? '59' : "{$partial[5]}9";
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:{$end_minute}:59";
          unset($end_minute);
          break;
        case '17': // Minute YYYY-MM-DDThh:mm:#
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:{$partial[5]}:00";
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:{$partial[5]}:59";
          break;
        case '18': // Second range YYYY-MM-DDThh:mm:s# (not iso 8601 but assume range intended)
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:{$partial[5]}:{$partial[6]}0";
          $end_second = (intval("{$partial[6]}9") > 59) ? '59' : "{$partial[6]}9";
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:{$partial[5]}:{$end_second}";
          unset($end_second);
          break;
        case '19': // Second YYYY-MM-DDThh:mm:ss# (just missing TimeZone assume UTC)
          $iso_dates[] = "{$partial[1]}-{$partial[2]}-{$partial[3]}T{$partial[4]}:{$partial[5]}:{$partial[6]}";
          break;
      }
      unset($partial);
      unset($pos);
      unset($match);
    }
  }

  $datetimes = array();
  foreach ($iso_dates as $i => $iso_date) {
    // Check and ignore invalid dates
    list($date, $time) = explode('T', $iso_date);
    list($year, $month, $day) = explode('-', $date);
    if (!checkdate($month, $day, $year)) {
      // @todo: Add entity type and id to watchdog message:
      watchdog('dspaced', 'Invalid date @iso_date parsed from DSpace field !field_name. Date value has been removed.',
        array('@iso_date' => $iso_date, '!field_name' => $field_name), WATCHDOG_WARNING);
      continue;
    }

    // We have a valid date so replace Z with difference from UTC
    if (substr($iso_date, -1) == 'Z') $iso_date = str_replace('Z', '+00:00', $iso_date);
    // @note If time values are larger than expected then when Date object is
    //   created the date will be adjusted e.g. hours value of 70 would move
    //   date forward a couple of days. We could make an assumption here
    //   and try to correct it... but instead lets just let it happen. If
    //   our code is right then it would be a data entry error and therefore
    //   should be corrected in DSpace not here.
    // list($hours, $minutes, $seconds) = explode(':', substr($time, 0, 8));
    if (($datetime = DateTime::createFromFormat(DATE_W3C, $iso_date)) ||
    ($datetime = DateTime::createFromFormat('Y-m-d\TH:i:s', $iso_date))) {
      $datetimes[] = $datetime;
    }
  }

  asort($datetimes);
  $start = array_shift($datetimes);
  $end = array_pop($datetimes);

  return array('start' => $start, 'end' => $end);

}

/**
 * Helper callback function for array_filter to determine whether to keep or
 * discard interval points from parsed ISO date string.
 *
 * @param String $interval_point
 *   Expected to be full or partial ISO 8601 date string.
 *
 * @return Boolean
 *   True if interval_point should be kept. False if interval point should be
 *   discarded.
 *
 * @note Using custom callback rather than default array_filter callback in
 *   order to preserve point values of '0'.
 *
 * @see http://php.net/manual/en/function.array-filter.php
 *
 */
function _dspaced_parse_date_filter($interval_point) {
  return ($interval_point === '0' || $interval_point);
}

/**
 * Helper function to unparse an array of URL parts.
 *
 * @param array $parsed_url
 *   A URL that has been parsed using parse_url.
 * @return string
 *   A URL imploded from the given array.
 */
function _dspaced_unparse_url($parsed_url) {
  foreach (array('scheme', 'user', 'pass', 'host', 'port', 'path', 'query', 'fragment') as $part) {
    ${$part} = isset($parsed_url[$part]) ? $parsed_url[$part] : '';
    if (${$part}) {
      switch ($part) {
        case 'scheme':
          ${$part} .= '://';
          break;
        case 'port':
        case 'pass':
          ${$part} = ':' . ${$part};
          break;
        case 'query':
          ${$part} = '?' . ${$part};
          break;
        case 'fragment':
          ${$part} = '#' . ${$part};
          break;
      }
    }
  }
  if ($user || $pass) $pass .= '@';
  return $scheme . $user . $pass . $host . $port . $path . $query . $fragment;
}


