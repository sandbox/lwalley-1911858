<?php

/**
 * @file dspaced.admin.inc
 * Defines admin forms and ctools wizard options.
 */

/**
 * Helper method to route calls to multistep form wizard callbacks.
 */
function dspaced_get_wizard($wizard, $step = NULL) {
  $function = "dspaced_{$wizard}_wizard";
  $wizard = $function($step);
  return $wizard;
}

/**
 * Generic submit callback for next action in a form wizard.
 */
function _dspaced_wizard_next(&$form_state) {
  _dspaced_set_page_cache($form_state['data_cache_name'], $form_state['data_cache']);
}

/**
 * Generic submit callback for finish action in a form wizard.
 */
function _dspaced_wizard_finish(&$form_state) {
  _dspaced_clear_page_cache($form_state['data_cache_name']);
}

/**
 * Generic submit callback for cancel action in a form wizard.
 */
function _dspaced_admin_settings_cancel(&$form_state) {
  _dspaced_clear_page_cache($form_state['data_cache_name']);
}

/**
 * Callback for admin/config/dspaced/settings.
 *
 * @note Called from dspaced_get_wizard, not directly by menu.
 *
 */
function dspaced_admin_settings_wizard($step) {
  ctools_include('wizard');
  ctools_include('object-cache');

  $form_info = array(
    'id' => 'dspaced_admin_settings',
    'path' => 'admin/config/dspaced/settings/%step',
    'return path' => 'admin/config/dspaced/settings',
    'cancel path' => 'admin/config/dspaced/settings',
    'show trail' => TRUE,
    'show back' => TRUE,
    'show cancel' => TRUE,
    'show return' => FALSE,
    'back text' => t('Back'),
    'next text' => t('Continue'),
    'cancel text' => t('Cancel'),
    'return text' => t('Save and return to status'),
    'finish text' => t('Confirm and save'),
    'ajax' => FALSE,
    'modal' => FALSE,
    'next callback' => '_dspaced_wizard_next',
    'finish callback' => '_dspaced_wizard_finish',
    'return callback' => '_dspaced_wizard_finish',
    'cancel callback' => '_dspaced_wizard_cancel',
    'order' => array(
      'status' => t('Status'),
      'connect' => t('Connect'),
      'communities' => t('Link communities'),
      'confirm' => t('Confirm and save'),
    ),
    'forms' => array(
      'status' => array(
        'form id' => 'dspaced_admin_settings_status_form',
        'title' => t('Status'),
      ),
      'connect' => array(
        'form id' => 'dspaced_admin_settings_connect_form',
        'title' => t('Connect'),
      ),
      'communities' => array(
        'form id' => 'dspaced_admin_settings_communities_form',
        'title' => t('Communities'),
      ),
      'confirm' => array(
        'form id' => 'dspaced_admin_settings_confirm_form',
        'title' => t('Confirm and save'),
      ),
    ),
  );

  $form_state['data_cache_name'] = 'settings';
  if (empty($step)) {
    _dspaced_clear_page_cache($form_state['data_cache_name']);
    $step = 'status';
  }

  $form_state['data_cache'] = _dspaced_get_page_cache($form_state['data_cache_name']);

  // @todo notification of unsaved data... use if report['changed']?

  $form = ctools_wizard_multistep_form($form_info, $step, $form_state);

  return $form;
}

/**
 * Callback for status subform of the multistep admin settings form.
 *
 * Status step is the first step of the form where user can see the status of
 * the repository and api connections and linked communities.
 */
function dspaced_admin_settings_status_form($form, &$form_state) {

  $baseurl = _dspaced_get_default_field_value('dspaced_repository_baseurl', $form_state,
    _dspaced_repository_baseurl()
  );
  $rest_baseurl = _dspaced_get_default_field_value('dspaced_repository_rest_baseurl', $form_state,
    _dspaced_repository_rest_baseurl()
  );
  $selected_communities = _dspaced_get_default_field_value(
    'dspaced_community_dspace_entity_ids',
    $form_state,
    variable_get('dspaced_community_dspace_entity_ids', array())
  );

  if (empty($baseurl) || empty($rest_baseurl)) {
    $connection_status = t("Not connected, continue to connect step to set up your repository connections.");
  }
  else {
    $connection_status = t("Repository connection says @repository_status; it's using <a href=\"@repository_url\">@repository_url</a>. API connection says @api_status; it's using <a href=\"@api_url\">@api_url</a>.",
      array(
        '@repository_status' => _dspaced_get_http_status_message($baseurl),
        '@repository_url' => $baseurl,
        '@api_status' => _dspaced_get_http_status_message($rest_baseurl . '/communities.xml', FALSE, TRUE),
        '@api_url' => $rest_baseurl,
      ));
  }

  $community_nodes = _dspaced_find_community_nodes();
  $community_jobs = _dspaced_tools_find_jobs_by_type_and_nid(array('dspaced_harvest'), array_keys($community_nodes));
  $communities = array();
  foreach ($community_nodes as $nid => $node) {
    $dspace_id = _dspaced_get_dspace_entity_id($node);
    $pending_deletion = NULL;
    if (!in_array($dspace_id, $selected_communities)) {
      // Community was either just deselected in the form, or it was previously
      // removed and is still in the process of clearing items so node still
      // exists.
      // @todo Perhaps we could set a flag on the community node when its
      //       deleted so we know for sure what is going on.
      $pending_deletion = t("Deselected and/or pending removal.");
      $pending_deletion = " [{$pending_deletion}]";
    }
    $last = $community_jobs[$nid]['last'];
    $communities[] = t("<strong><a href=\"@url\">@name</a>:</strong> last automated harvest ran @interval.", array(
      '@name' => $node->title,
      '@url' => url("node/{$node->nid}"),
      '@interval' => ($last > 0) ? format_interval(time() - $last) . ' ' . t('ago') : t('never'),
    )) . $pending_deletion;
    unset($last);
    unset($dspace_id);
    unset($pending_deletion);
  }

  $result = db_query('SELECT COUNT(*) count FROM {node} WHERE node.type = :type',
    array(':type' => 'dspaced_entities_item'))->fetch();
  $imported_items_count = $result->count;

  $imported_items_status = array(t("@count items imported.", array('@count' => $imported_items_count)));
  if ($imported_items_count > 0 && ($imported_items_last_item = _dspaced_find_lastmodified_item_node())) {
    $field = field_get_items('node', $imported_items_last_item, 'dspace_lastmodified');
    $datetime = field_view_value('node',
      $imported_items_last_item,
      'dspace_lastmodified',
      $field[0],
      array('settings' => array('format_type' => 'long'))
    );
    $imported_items_status[] = t("Last item to be updated was <a href=\"@url\">@title</a>. It was last modified in the repository on !datetime.", array('@title' => $imported_items_last_item->title,
      '@url' => url('node/' . $imported_items_last_item->nid),
      '!datetime' => render($datetime),
    ));
    unset($field);
    unset($date);
  }

  $form['dspaced_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
    '#description' => theme('dspaced_status', array(
      'connection_status' => array('#markup' => $connection_status),
      'linked_communities_count' => count($community_nodes),
      'linked_communities_nodes' => $community_nodes,
      'linked_communities_jobs' => $community_jobs,
      'linked_communities_status' => format_plural(count($community_nodes), '1 linked community', '@count linked communities'),
      'linked_communities_list' => array(
        '#theme' => 'item_list',
        '#items' => $communities,
      ),
      'imported_items_count' => $imported_items_count,
      'imported_items_last_item' => $imported_items_last_item,
      'imported_items_status' => array(
        '#markup' => implode($imported_items_status, ' '),
      )
    )),
  );

  $form['buttons']['cancel']['#access'] = FALSE;

  return $form;
}

/**
 * Callback for connect subform of the multistep admin settings form.
 *
 * Connect step is the first step of the form where users enter/update
 * repository and api URLs and connect to repository.
 */
function dspaced_admin_settings_connect_form($form, &$form_state) {

  $baseurl = _dspaced_get_default_field_value(
    'dspaced_repository_baseurl',
    $form_state,
    _dspaced_repository_baseurl()
  );

  $rest_baseurl = _dspaced_get_default_field_value(
    'dspaced_repository_rest_baseurl',
    $form_state,
    _dspaced_repository_rest_baseurl()
  );

  $form['dspaced_repository'] = array(
    '#type' => 'fieldset',
    '#title' => t('Connect'),
  );

  // Repository URL
  $form['dspaced_repository']['dspaced_repository_baseurl'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Repository URL'),
    '#default_value' => $baseurl,
    '#description' => t("Enter the address of your DSpace repository's base URL e.g. http://repository.edu. The base URL is used to build bitstream URLs during import. Bitstream URLs are hardcoded to use the default handle/filename path which is appended to the base URL e.g. http://repository.edu/bitstream/handle/12345/1234/filename.ext"),
    '#maxlength' => 100,
  );
  if (!empty($baseurl)) {
    $form['dspaced_repository']['dspaced_repository_baseurl']['#suffix'] = t("<strong>Connection status:</strong> Repository says @status.", array('@status' => _dspaced_get_http_status_message($baseurl)));
  }

  // API URL
  $form['dspaced_repository']['dspaced_repository_rest_baseurl'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('API URL'),
    '#default_value' => $rest_baseurl,
    '#description' => t("Enter the address of your DSpace repository's " .
                        "API base domain and path e.g. http://repository.edu/rest."),
    '#maxlength' => 100,
  );

  if (!empty($rest_baseurl)) {
    $form['dspaced_repository']['dspaced_repository_rest_baseurl']['#suffix'] = t("<strong>Connection status</strong>: API says @status.", array('@status' => _dspaced_get_http_status_message("{$rest_baseurl}/communities.xml", FALSE, TRUE)));
  }

  return $form;
}

/**
 * Validation handler for connect step of multistep admin settings form.
 */
function dspaced_admin_settings_connect_form_validate($form, &$form_state) {
  $urls = array(
    'dspaced_repository_baseurl' => array(
      'url' => $form_state['values']['dspaced_repository_baseurl'],
      'message' => _dspaced_get_http_status_message($form_state['values']['dspaced_repository_baseurl']),
      'name' => t('the repository'),
    ),
    'dspaced_repository_rest_baseurl' => array(
      'url' => $form_state['values']['dspaced_repository_rest_baseurl'],
      'message' => _dspaced_get_http_status_message(
        "{$form_state['values']['dspaced_repository_rest_baseurl']}/communities.xml",
        FALSE, TRUE
      ),
      'name' => t('the API'),
    )
  );
  foreach ($urls as $field => $data) {
    if (!_dspaced_http_status_message_is_ok($data['message'])) {
      form_set_error($field, t("Looks like there is a bad connection with @name it returned @message. Is <a href=\"@url\">@url</a> the right URL?", array('@name' => $data['name'], '@url' => $data['url'], '@message' => trim($data['message']))));
    }
  }
}

/**
 * Submit handler for connect step of multistep admin settings form.
 */
function dspaced_admin_settings_connect_form_submit($form, &$form_state) {
  $fields = array(
    'dspaced_repository_baseurl',
    'dspaced_repository_rest_baseurl',
  );
  _dspaced_store_field_values($fields, $form_state);
}

/**
 * Callback for communities subform of the multistep admin settings form.
 *
 * Communities step is the second step of the form where users choose which
 * DSpace communities they want to import items from.
 */
function dspaced_admin_settings_communities_form($form, &$form_state) {

  module_load_include('inc', 'dspaced', 'dspaced.feeds');

  $rest_baseurl = _dspaced_get_default_field_value(
    'dspaced_repository_rest_baseurl',
    $form_state,
    _dspaced_repository_rest_baseurl()
  );
  $selected_communities = _dspaced_get_default_field_value(
    'dspaced_community_dspace_entity_ids',
    $form_state,
    variable_get('dspaced_community_dspace_entity_ids', array())
  );

  $form['dspaced_communities'] = array(
    '#type' => 'fieldset',
    '#title' => t('Communities'),
  );

  $fetched_communities = _dspaced_fetch_communities($rest_baseurl);

  $form['dspaced_communities']['dspaced_communities_count'] = array(
    '#markup' => t('Found @communities in your repository.',
      array('@communities' => format_plural(count($fetched_communities),
      '1 community', '@count communities'))),
  );

  if (isset($fetched_communities) && $fetched_communities) {
    $form['dspaced_communities']['dspaced_community_dspace_entity_ids'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Link communities'),
      '#default_value' => $selected_communities,
      '#options' => $fetched_communities,
      '#description' => t('Select which communities items should be imported from. Please note that if you deselect a community, all data imported from that community, including images, will be deleted.'),
    );
  }

  return $form;
}

/**
 * Submit handler for communities step of multistep admin settings form.
 */
function dspaced_admin_settings_communities_form_submit($form, &$form_state) {
  $fields = array('dspaced_community_dspace_entity_ids');
  _dspaced_store_field_values($fields, $form_state);
  // Store community names for displaying on the confirmation form
  $form_state['data_cache']->fetched_communities =
    $form['dspaced_communities']['dspaced_community_dspace_entity_ids']['#options'];
}

/**
 * Callback for confirmation subform of the multistep admin settings form.
 *
 * Confirmation step is the last step of the form where users confirm which
 * DSpace communities they want to import items from.
 */
function dspaced_admin_settings_confirm_form($form, &$form_state) {

  $form['dspaced_confirm'] = array(
    '#type' => 'fieldset',
    '#title' => t('Confirm settings'),
  );

  // Get previously saved values so we can report on what changes were made, if any.
  $saved_values = array(
    'dspaced_repository_baseurl' => _dspaced_repository_baseurl(),
    'dspaced_repository_rest_baseurl' => _dspaced_repository_rest_baseurl(),
    'dspaced_community_dspace_entity_ids' => variable_get('dspaced_community_dspace_entity_ids', array()),
  );

  $report_items = array();

  // Store changed fields to cache so we can save them in submit call back
  $form_state['data_cache']->report = array(
    'changed' => array(),
    'communities' => array(
      'create' => array(),
      'delete' => array(),
    ),
  );
  $report = &$form_state['data_cache']->report;

  // Figure out if any values have changed so we can report on them.
  foreach ($saved_values as $field => $saved_value) {
    if ((is_array($saved_value) && array_diff_assoc($form_state['data_cache']->$field, $saved_value)) || (is_scalar($saved_value) && ($saved_value != $form_state['data_cache']->$field))) {
      $report['changed'][] = $field;
    }
  }

  // Report on changes to connection settings.
  if (in_array('dspaced_repository_baseurl', $report['changed'])) {
    $report_items[] = t("Repository base URL will be updated to <a href=\"@url\">@url</a>.", array('@url' =>$form_state['data_cache']->dspaced_repository_baseurl));
  }
  else {
    $report_items[] = t("Repository base URL <a href=\"@url\">@url</a> remains the same and will not be updated.", array('@url' =>$form_state['data_cache']->dspaced_repository_baseurl));
  }
  if (in_array('dspaced_repository_rest_baseurl', $report['changed'])) {
    $report_items[] = t("API base URL will be updated to <a href=\"@url\">@url</a>.", array('@url' =>$form_state['data_cache']->dspaced_repository_rest_baseurl));
  }
  else {
    $report_items[] = t("API base URL <a href=\"@url\">@url</a> remains the same and will not be updated.", array('@url' =>$form_state['data_cache']->dspaced_repository_rest_baseurl));
  }

  // Report which communities need creating and which need deleting, if any.
  if (in_array('dspaced_community_dspace_entity_ids', $report['changed'])) {
    foreach ($form_state['data_cache']->$field as $id => $selected) {
      if ($selected && !in_array($id, $saved_value)) {
        // Only create communities if they're not already saved in Drupal.
        $report['communities']['create'][$id] =
          $form_state['data_cache']->fetched_communities[$id];
      }
      elseif (!$selected && in_array($id, $saved_value)) {
        // Only delete communities if they exist in Drupal
        $report['communities']['delete'][$id] =
          $form_state['data_cache']->fetched_communities[$id];
      }
    }
  }
  if (!$report['communities']['create'] && !$report['communities']['delete']) {
    $report_items[] = t("No changes will be made to communities.");
  }
  if ($report['communities']['create']) {
    $report_items[] = format_plural(count($report['communities']['create']),
      "The following community and its items will be imported: <strong>@communities</strong>.",
      "The following @count communities and their items will be imported: <strong>@communities</strong>.",
      array('@communities' => implode($report['communities']['create'], '; '))
    );
  }
  if ($report['communities']['delete']) {
    $report_items[] = format_plural(count($report['communities']['delete']),
      "The following community and its items will be deleted: <strong>@communities</strong>.",
      "The following @count communities and their items will be deleted: <strong>@communities</strong>.",
      array('@communities' => implode($report['communities']['delete'], '; '))
    );
  }

  $form['dspaced_confirm']['#description'] = theme('item_list', array('items' => $report_items));

  return $form;
}

/**
 * Submit handler for communities step of multistep admin settings form.
 */
function dspaced_admin_settings_confirm_form_submit($form, &$form_state) {
  $messages = array();
  $report = $form_state['data_cache']->report;

  if ($report['changed']) {
    // Save changed values to database.
    foreach ($report['changed'] as $field) {
      variable_set($field, $form_state['data_cache']->$field);
      switch ($field) {
        case 'dspaced_repository_baseurl':
          $messages[] = t("Repository base URL has been updated to <a href=\"@url\">@url</a>.",
            array('@url' =>$form_state['data_cache']->dspaced_repository_baseurl));
          break;
        case 'dspaced_repository_rest_baseurl':
          $messages[] = t("API base URL has been updated to <a href=\"@url\">@url</a>.",
            array('@url' =>$form_state['data_cache']->dspaced_repository_rest_baseurl));
          break;
      }
    }
  }
  else {
    $messages[] = t("No changes were made to DSpaced settings.");
  }

  // Create and/or delete communities and their items if needed.
  if ($report['communities']['create'] || $report['communities']['delete']) {
    module_load_include('inc', 'dspaced', 'dspaced.feeds');
  }
  else {
    $messages[] = t("No changes were made to communities.");
  }

  if ($report['communities']['create']) {
    // Import newly selected communities.
    foreach (array_keys($report['communities']['create']) as $dspace_id) {
      _dspaced_feeds_source_standalone_import('dspaced_community', "{$form_state['data_cache']->dspaced_repository_rest_baseurl}/communities/{$dspace_id}.xml");
    }
    $messages[] = format_plural(
      count($report['communities']['create']),
      "Added community @communities and scheduled import of its items.",
      "Added communities @communities and scheduled import of their items.",
      array('@communities' => implode($report['communities']['create'], '; '))
    );
  }

  if ($report['communities']['delete']) {
    // @fixme Clear won't remove harvested items because once the dspaced_item
    //        importer is triggered the feeds_item table id is updated from
    //        dspaced_harvest to dspaced_item. This is because we made guids
    //        in feeds_item table unique to prevent duplicate nodes with the
    //        same dspace id (see https://drupal.org/node/1233142).
    //        To fix this clearing problem we'll need to either  manually
    //        track which items belong to which communities. Or update feeds
    //        module to support multiple importers creating/updating a node.
    //        Also note that items can belong to multiple communities, so if
    //        the site has more than one linked community then an item might
    //        belong to both and therefore should not necessarily be deleted
    //        when one of the communities is deleted.
    $nodes = _dspaced_find_nodes_by_dspace_id('dspaced_entities_community', array_keys($report['communities']['delete']));
    foreach ($nodes as $node) {
      _dspaced_feeds_feed_node_clear('dspaced_harvest', $node->nid, FALSE);
    }
    $messages[] = format_plural(
      count($report['communities']['delete']),
      "Scheduled removal of community @communities and its items. Unfortunately we might not be able to remove all of the community's items. We're trying to find a fix, but in the meantime you'll have to remove any remaining item nodes yourself.",
      "Scheduled removal of communities @communities and their items. Unfortunately we might not be able to remove all of the communities' items. We're trying to find a fix, but in the meantime you'll have to remove any remaining item nodes yourself.",
      array('@communities' => implode($report['communities']['delete'], '; '))
    );
  }

  if ($messages) {
    drupal_set_message(implode($messages, ' '));
  }
}

/**
 * Helper function to get default field values for admin settings sub forms.
 *
 * @param string $field
 *   Field name to check for cached values.
 *
 * @param array $form_state
 *   Form cache passed by reference.
 *
 * @param mixed $default
 *   Default value to use if cached value not found.
 */
function _dspaced_get_default_field_value($field, $form_state, $default) {
  if (isset($form_state['data_cache']->$field)) {
    return $form_state['data_cache']->$field;
  }
  return $default;
}

/**
 * Helper function to store field values in form state.
 *
 * @param array $fields
 *   Array of field names.
 *
 * @param array $form_state
 *   Form cache passed by reference.
 */
function _dspaced_store_field_values($fields, &$form_state) {
  foreach ($fields as $name) {
    $form_state['data_cache']->$name = $form_state['values'][$name];
  }
}
