ABOUT DSPACED TOOLS
-------------------

DSpaced Tools module provides additional Feeds importers that can be used
to import items from a DSpace repository using the DSpaceTools API
(https://github.com/mbl-cli/DspaceTools).

The DSpaceTools API application provides additional API operations that allow us
to find items that have been updated in the last hour, or any other time
period we choose.


DEPENDENCIES
------------
 * An instance of DSpaceTools application must be installed and able to
   query your DSpace repository. DSpaceTools is an open source project and
   can be found on GitHub (https://github.com/mbl-cli/DspaceTools).

LIMITATIONS OF DSPACE (1.7.x) API
---------------------------------

The default DSpace API (1.7.x) provides a 'harvest' operation which
'Returns items created/modified within specified time range'. However, it only
supports 'startdate' and 'enddate' parameters, it does not support timestamps.
The lack of support for timestamps means that, when using the DSpace API,
we can only harvest items that have been updated in the last day.

Another limitation of the DSpace API's 'harvest' option is that it does not
provide last modified date of items when using the 'idOnly=true' parameter. So
we cannot filter out items that do not fit in our desired time range. Also, we
cannot use 'idOnly=false' because, if users are bulk uploading then a response
containing one days worth of items could be too big for DSpace to handle
(results in 500 error). The DSpace API 'harvest' option appears to support
pagination of results which would mean smaller responses, but it doesn't seem
to work.

TODO
----
 * Look into mimicking priority queues by assigning more time to workers of
   high priority queues. For example, we could create custom queue for item
   updates and spend give workers of that queue more time to process items and
   still have harvest and self updates running with lower priority (less time)
   and with longer periods between updates.


