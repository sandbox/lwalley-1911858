<?php

/**
 * @file dspaced_tools.admin.inc
 * DSpaced Tools administration settings.
 */
/**
 * Helper function to implement form alter on
 * /admin/config/dspaced/settings/status.
 */
function _dspaced_tools_form_dspaced_admin_settings_status_form_alter(&$form, &$form_state, $form_id) {
  // @todo Show status of dspaced_tools settings and item updates.
}
/**
 * Helper function to implement form alter on
 * /admin/config/dspaced/settings/confirm.
 */
function _dspaced_tools_form_dspaced_admin_settings_confirm_form_alter(&$form, &$form_state, $form_id) {
  // @todo Report on dspaced_tools settings changes
  $form['#submit'][] = '_dspaced_tools_dspaced_admin_settings_confirm_form_submit';
}

/**
 * Helper function to implement form alter on
 * /admin/config/dspaced/settings/connect.
 */
function _dspaced_tools_form_dspaced_admin_settings_connect_form_alter(&$form, &$form_state, $form_id) {

  date_default_timezone_set('UTC');

  $baseurl = _dspaced_get_default_field_value('dspaced_repository_baseurl', $form_state,
    _dspaced_repository_baseurl()
  );
  $rest_baseurl = _dspaced_get_default_field_value('dspaced_repository_rest_baseurl', $form_state,
    _dspaced_repository_rest_baseurl()
  );
  $timestamp = _dspaced_get_default_field_value(
    'dspaced_tools_item_updates_timestamp', $form_state,
    _dspaced_tools_item_updates_timestamp()
  );
  $enabled = _dspaced_get_default_field_value(
    'dspaced_tools_item_updates_enabled', $form_state,
    _dspaced_tools_item_updates_enabled()
  );
  $api_key = _dspaced_get_default_field_value(
    'dspaced_tools_api_key', $form_state,
    _dspaced_tools_api_key()
  );
  $api_digest = _dspaced_get_default_field_value(
    'dspaced_tools_item_updates_api_digest', $form_state,
    _dspaced_tools_item_updates_api_digest()
  );

  $datetime = _dspaced_tools_timestamp_is_valid($timestamp);

  $form['dspaced_tools'] = array(
    '#type' => 'fieldset',
    '#title' => t('DSpace Tools'),
  );

  $form['dspaced_tools']['dspaced_tools_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => $api_key,
  );

  $form['dspaced_tools']['dspaced_tools_item_updates_api_digest'] = array(
    '#type' => 'textfield',
    '#title' => t('API digest'),
    '#default_value' => $api_digest,
  );

  $form['dspaced_tools']['dspaced_tools_item_updates_timestamp'] = array(
    '#type' => 'textfield',
    '#title' => t('Look for updates since'),
    '#default_value' => $timestamp,
  );

  $form['dspaced_tools']['dspaced_tools_item_updates_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable item updates'),
    '#description' => t("Determines whether or not the item updates importer is active. Check box to enable item updates - requires a valid timestamp. Uncheck to stop item updates and deactivate the item updates importer."),
    '#default_value' => $enabled,
  );


  if ($datetime) {
    $cron_last = variable_get('cron_last');
    $form['dspaced_tools']['dspaced_tools_item_updates_timestamp']['#description'] =
      t("The timestamp for the next import is @timestamp. The timestamp determines how far back to look for updates in all connected DSpace communities. The timestamp is managed automatically by the importer and should not be edited. However, if you do manually edit the timestamp keep in mind that you might skip some updates if you bring the timestamp forward. The importer runs automatically as a custom cron job. Cron was last run @cron_last_interval ago.", array('@timestamp' => $datetime->format('Y-m-d H:i:s T'), '@cron_last_interval' => format_interval(time() - $cron_last)));
  }
  else {
    $form['dspaced_tools']['dspaced_tools_item_updates_timestamp']['#description'] =
      t("Set a timestamp to activate the DSpace Tools item importer Use the format '!timestamp_format'. The timestamp determines how far back to look for updates in all connected communities, and once set will be managed automatically by the importer.", array('!timestamp_format' => date('Y-m-d H:i:s T')));
  }

  // Check item api connection if user is trying to enable item updates
  if ($enabled) {
    $url = "{$rest_baseurl}/updates/items.xml" .
           "?api_key={$api_key}" .
           "&api_digest={$api_digest}" .
           "&timestamp=" . urlencode('3000-01-01 00:00:00 UTC');
    $api_item_updates_status_message = _dspaced_get_http_status_message($url);
    if (!isset($form['dspaced_repository']['dspaced_repository_rest_baseurl']['#suffix'])) {
      $form['dspaced_repository']['dspaced_repository_rest_baseurl']['#suffix'] = '';
    }
    $form['dspaced_repository']['dspaced_repository_rest_baseurl']['#suffix'] .= ' ' .
      t("API says @item_updates_api_status for Dspace Tools item updates.",
        array('@item_updates_api_status' => $api_item_updates_status_message));

    if ($enabled && !_dspaced_http_status_message_is_ok($api_item_updates_status_message)) {
      $form['dspaced_tools']['#description'] =
        t("<strong>Warning:</strong> it looks like the API doesn't support DSpace Tools item updates. It says @item_updates_api_status. Make sure the API URL is pointing to the DSpace Tools service, and the service is available.",
        array('@item_updates_api_status' => $api_item_updates_status_message));
    }
  }

  $form['#validate'][] = '_dspaced_tools_dspaced_admin_settings_connect_form_validate';
  $form['#submit'][] = '_dspaced_tools_dspaced_admin_settings_connect_form_submit';
}

/**
 * Additional validation callback for DSpaced settings connect form.
 */
function _dspaced_tools_dspaced_admin_settings_connect_form_validate($form, &$form_state) {
  // Only validate certain form elements if importer is enabled
  if ($form_state['values']['dspaced_tools_item_updates_enabled']) {
    $rest_baseurl = $form_state['values']['dspaced_repository_rest_baseurl'];
    $timestamp = $form_state['values']['dspaced_tools_item_updates_timestamp'];
    $api_digest = $form_state['values']['dspaced_tools_item_updates_api_digest'];
    $api_key = $form_state['values']['dspaced_tools_api_key'];
    if (!_dspaced_tools_timestamp_is_valid($timestamp)) {
      form_set_error('dspaced_tools_item_updates_timestamp',
        t("Timestamp is missing or not in the expected format. The expected format is '!timestamp_format'. A valid timestamp is required before enabling item updates.", array('!timestamp_format' => date('Y-m-d H:i:s T'))));
    }
    if (!_dspaced_tools_api_digest_or_key_looks_valid($api_digest)) {
      form_set_error('dspaced_tools_item_updates_api_digest',
        t("API digest is missing or not in the expected format. The expected format is 8 alphanumeric characters. API digest is required before enabling item updates.")
      );
    }
    if (!_dspaced_tools_api_digest_or_key_looks_valid($api_key)) {
      form_set_error('dspaced_tools_api_key',
        t("API key is missing or not in the expected format. The expected format is 8 alphanumeric characters. API key is required before enabling item updates.")
      );
    }
    $url = "{$rest_baseurl}/updates/items.xml" .
           "?api_key={$api_key}" .
           "&api_digest={$api_digest}" .
           "&timestamp=" . urlencode('3000-01-01 00:00:00 UTC');
    $api_item_updates_status_message = _dspaced_get_http_status_message($url);
    if (!_dspaced_http_status_message_is_ok($api_item_updates_status_message)) {
      form_set_error('dspaced_repository_rest_baseurl',
        t("Can't connect to DSpace Tools item updates. API says @item_updates_api_status for Dspace Tools item updates. A working API is needed before enabling item updates.",
        array('@item_updates_api_status' => $api_item_updates_status_message)));

    }
  }
}
/**
 * Additional submit callback for DSpaced settings connect form.
 */
function _dspaced_tools_dspaced_admin_settings_connect_form_submit($form, &$form_state) {
  $fields = array(
    'dspaced_tools_api_key',
    'dspaced_tools_item_updates_api_digest',
    'dspaced_tools_item_updates_timestamp',
    'dspaced_tools_item_updates_enabled',
  );
  _dspaced_store_field_values($fields, $form_state);
}

/**
 * Additional submit callback for DSpaced settings connect form.
 */
function _dspaced_tools_dspaced_admin_settings_confirm_form_submit($form, &$form_state) {

  // @todo Only updates things that have changed, and improve feedback.
  $fields = array(
    'dspaced_tools_api_key',
    'dspaced_tools_item_updates_api_digest',
    'dspaced_tools_item_updates_timestamp',
    'dspaced_tools_item_updates_enabled',
  );
  foreach ($fields as $field) {
    if (isset($form_state['data_cache']->$field)) {
      variable_set($field, $form_state['data_cache']->$field);
    }
  }
  drupal_set_message(t("Saved DSpace Tools settings."));

}
