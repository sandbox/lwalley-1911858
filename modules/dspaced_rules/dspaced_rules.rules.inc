<?php

/**
 * @file
 * Contains custom actions for Rules.
 */

/**
 * Implements hook_rules_action_info().
 */
function dspaced_rules_rules_action_info() {
  return array(
    'dspaced_rules_action_import_item' => array(
      'label' => t('Import DSpace item'),
      'group' => t('DSpaced'),
      'parameter' => array(
        'dspaced_rules_parameter_item_node' => array(
          'type' => 'node',
          'label' => t('DSpace item node'),
        ),
      ),
    ),
  );
}

/**
 * Callback for rules action.
 *
 * Triggers import of node using Feeds.
 *
 * @param $node
 *   Feeds node object to run import on. Node must be attached to a Feed.
 */
function dspaced_rules_action_import_item($node) {
  if (!is_object($node)) {
    drupal_set_message(t('Imported cancelled. DSpace item node is missing.', 'error'));
    return false;
  }
  if ($node->type != 'dspaced_entities_item') {
    drupal_set_message(t('Import aborted. Node is not a DSpace item.', 'error'));
    return false;
  }
  $source = feeds_source('dspaced_item', $node->nid);
  $source->startImport();
}


