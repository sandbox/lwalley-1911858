<?php
/**
 * @file
 * dspaced_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function dspaced_rules_default_rules_configuration() {
  $items = array();
  $items['rules_dspaced_rules_component_import_item'] = entity_import('rules_config', '{ "rules_dspaced_rules_component_import_item" : {
      "LABEL" : "Import DSpace Item Node",
      "PLUGIN" : "action set",
      "TAGS" : [ "DSpaced" ],
      "REQUIRES" : [ "dspaced_rules" ],
      "USES VARIABLES" : { "dspaced_item_node" : { "label" : "Item Node", "type" : "node" } },
      "ACTION SET" : [
        { "dspaced_rules_action_import_item" : { "dspaced_rules_parameter_item_node" : [ "dspaced-item-node" ] } }
      ]
    }
  }');
  return $items;
}
